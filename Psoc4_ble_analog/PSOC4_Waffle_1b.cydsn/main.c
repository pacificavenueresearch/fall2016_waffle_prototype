/*******************************************************************************
* File Name: main.c
*
* Version:   1.0
*
* Description:
*  direct DMA from ADC to IDAC prototype 
*
*  
*
********************************************************************************
* Copyright 2016, Pacific Ave Research, LLC 2016. All rights reserved.
* This software is owned by Cypress Semiconductor Corporation and is protected
* by and subject to worldwide patent and copyright laws and treaties.
* Therefore, you may use this software only as provided in the license agreement
* accompanying the software package from which you obtained this software.
* CYPRESS AND ITS SUPPLIERS MAKE NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
* WITH REGARD TO THIS SOFTWARE, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT,
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
*******************************************************************************/
#include <project.h>

/*******************************************************************************
*  Helipful data manipulation defines from psoc5 pass thru example
*******************************************************************************/

/* Get 8 bits of 16 bit value. */
#define LO8(x)                  ((uint8) ((x) & 0xFFu))
#define HI8(x)                  ((uint8) ((uint16)(x) >> 8))

/* Get 16 bits of 32 bit value. */
#define LO16(x)                 ((uint16) ((x) & 0xFFFFu))
#define HI16(x)                 ((uint16) ((uint32)(x) >> 16))



#define    IDAC_VAL     (0x33u)
#define    IDAC_CURRENT (120u)



 

/*******************************************************************************
*   Function Prototypes
*******************************************************************************/



/*******************************************************************************
*   Global Variables
*******************************************************************************/



/* ADC measurement status and result */
int16   adcSample = 0;
int16   adcSampleQpoint = 0;
uint8   idacSet = 0x33;
int32   newRegisterValue = 0x0;

 const uint8 sinetable[ 32 ] = {     127, 152, 176, 198, 217, 233, 245, 252,
                                    254, 252, 245, 233, 217, 198, 176, 152,
                                    128, 103,  79,  57,  38,  22,  10,   3,
                                      0,   3,  10,  22,  38,  57,  79, 103 };

uint8 sineptr = 0;
uint8 freqCount = 0;
uint8 modulate = 0;


/*******************************************************************************
* Function Name: main
********************************************************************************
*
* Summary:
*  Sets DMA pointers.
*  Starts all the components. 
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
int main()
{
        /* Start Opamp component */


  OpampLPFilter_Start();
  OpampLPFilter_SetPower(OpampLPFilter_HIGH_POWER);

    
    /* Initialize ADC. Conversion is not enabled yet. */
    ADC_Start();
    
    /* Start the IDAC component */
    IDAC8_Start();

  
    /* Configure DMA source and destination locations to transfer from
    * ADC data register to memory variable.
    */    
   

     DMA_Start((void *)ADC_SAR_CHAN0_RESULT_PTR, (void *)&adcSample);

    /* Start ADC conversion. */
    ADC_StartConvert();
  
    adcSampleQpoint=0x3E;
    
    for(;;)
    {
        
                idacSet=0*(adcSample-adcSampleQpoint)*modulate/32+adcSample;
                newRegisterValue = ((IDAC8_IDAC_CONTROL_REG & (~(uint32)IDAC8_IDAC_VALUE_MASK)) | idacSet );
                IDAC8_IDAC_CONTROL_REG = newRegisterValue;
                if (freqCount == 32) {
                modulate=sinetable[sineptr++];
                freqCount=0;
                }
                
                freqCount++;
                if ( sineptr == 32 ) sineptr = 0;
          
        
        
    }
}





/* [] END OF FILE */
