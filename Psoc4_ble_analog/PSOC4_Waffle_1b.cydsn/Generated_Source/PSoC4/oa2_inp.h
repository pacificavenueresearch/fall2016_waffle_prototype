/*******************************************************************************
* File Name: oa2_inp.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_oa2_inp_H) /* Pins oa2_inp_H */
#define CY_PINS_oa2_inp_H

#include "cytypes.h"
#include "cyfitter.h"
#include "oa2_inp_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} oa2_inp_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   oa2_inp_Read(void);
void    oa2_inp_Write(uint8 value);
uint8   oa2_inp_ReadDataReg(void);
#if defined(oa2_inp__PC) || (CY_PSOC4_4200L) 
    void    oa2_inp_SetDriveMode(uint8 mode);
#endif
void    oa2_inp_SetInterruptMode(uint16 position, uint16 mode);
uint8   oa2_inp_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void oa2_inp_Sleep(void); 
void oa2_inp_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(oa2_inp__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define oa2_inp_DRIVE_MODE_BITS        (3)
    #define oa2_inp_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - oa2_inp_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the oa2_inp_SetDriveMode() function.
         *  @{
         */
        #define oa2_inp_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define oa2_inp_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define oa2_inp_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define oa2_inp_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define oa2_inp_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define oa2_inp_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define oa2_inp_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define oa2_inp_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define oa2_inp_MASK               oa2_inp__MASK
#define oa2_inp_SHIFT              oa2_inp__SHIFT
#define oa2_inp_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in oa2_inp_SetInterruptMode() function.
     *  @{
     */
        #define oa2_inp_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define oa2_inp_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define oa2_inp_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define oa2_inp_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(oa2_inp__SIO)
    #define oa2_inp_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(oa2_inp__PC) && (CY_PSOC4_4200L)
    #define oa2_inp_USBIO_ENABLE               ((uint32)0x80000000u)
    #define oa2_inp_USBIO_DISABLE              ((uint32)(~oa2_inp_USBIO_ENABLE))
    #define oa2_inp_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define oa2_inp_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define oa2_inp_USBIO_ENTER_SLEEP          ((uint32)((1u << oa2_inp_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << oa2_inp_USBIO_SUSPEND_DEL_SHIFT)))
    #define oa2_inp_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << oa2_inp_USBIO_SUSPEND_SHIFT)))
    #define oa2_inp_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << oa2_inp_USBIO_SUSPEND_DEL_SHIFT)))
    #define oa2_inp_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(oa2_inp__PC)
    /* Port Configuration */
    #define oa2_inp_PC                 (* (reg32 *) oa2_inp__PC)
#endif
/* Pin State */
#define oa2_inp_PS                     (* (reg32 *) oa2_inp__PS)
/* Data Register */
#define oa2_inp_DR                     (* (reg32 *) oa2_inp__DR)
/* Input Buffer Disable Override */
#define oa2_inp_INP_DIS                (* (reg32 *) oa2_inp__PC2)

/* Interrupt configuration Registers */
#define oa2_inp_INTCFG                 (* (reg32 *) oa2_inp__INTCFG)
#define oa2_inp_INTSTAT                (* (reg32 *) oa2_inp__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define oa2_inp_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(oa2_inp__SIO)
    #define oa2_inp_SIO_REG            (* (reg32 *) oa2_inp__SIO)
#endif /* (oa2_inp__SIO_CFG) */

/* USBIO registers */
#if !defined(oa2_inp__PC) && (CY_PSOC4_4200L)
    #define oa2_inp_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define oa2_inp_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define oa2_inp_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define oa2_inp_DRIVE_MODE_SHIFT       (0x00u)
#define oa2_inp_DRIVE_MODE_MASK        (0x07u << oa2_inp_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins oa2_inp_H */


/* [] END OF FILE */
