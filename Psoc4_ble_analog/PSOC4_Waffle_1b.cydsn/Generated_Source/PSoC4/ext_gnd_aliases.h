/*******************************************************************************
* File Name: ext_gnd.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_ext_gnd_ALIASES_H) /* Pins ext_gnd_ALIASES_H */
#define CY_PINS_ext_gnd_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define ext_gnd_0			(ext_gnd__0__PC)
#define ext_gnd_0_PS		(ext_gnd__0__PS)
#define ext_gnd_0_PC		(ext_gnd__0__PC)
#define ext_gnd_0_DR		(ext_gnd__0__DR)
#define ext_gnd_0_SHIFT	(ext_gnd__0__SHIFT)
#define ext_gnd_0_INTR	((uint16)((uint16)0x0003u << (ext_gnd__0__SHIFT*2u)))

#define ext_gnd_INTR_ALL	 ((uint16)(ext_gnd_0_INTR))


#endif /* End Pins ext_gnd_ALIASES_H */


/* [] END OF FILE */
