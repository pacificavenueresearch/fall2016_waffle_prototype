/*******************************************************************************
* File Name: oa2_inn.c  
* Version 2.20
*
* Description:
*  This file contains APIs to set up the Pins component for low power modes.
*
* Note:
*
********************************************************************************
* Copyright 2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "oa2_inn.h"

static oa2_inn_BACKUP_STRUCT  oa2_inn_backup = {0u, 0u, 0u};


/*******************************************************************************
* Function Name: oa2_inn_Sleep
****************************************************************************//**
*
* \brief Stores the pin configuration and prepares the pin for entering chip 
*  deep-sleep/hibernate modes. This function must be called for SIO and USBIO
*  pins. It is not essential if using GPIO or GPIO_OVT pins.
*
* <b>Note</b> This function is available in PSoC 4 only.
*
* \return 
*  None 
*  
* \sideeffect
*  For SIO pins, this function configures the pin input threshold to CMOS and
*  drive level to Vddio. This is needed for SIO pins when in device 
*  deep-sleep/hibernate modes.
*
* \funcusage
*  \snippet oa2_inn_SUT.c usage_oa2_inn_Sleep_Wakeup
*******************************************************************************/
void oa2_inn_Sleep(void)
{
    #if defined(oa2_inn__PC)
        oa2_inn_backup.pcState = oa2_inn_PC;
    #else
        #if (CY_PSOC4_4200L)
            /* Save the regulator state and put the PHY into suspend mode */
            oa2_inn_backup.usbState = oa2_inn_CR1_REG;
            oa2_inn_USB_POWER_REG |= oa2_inn_USBIO_ENTER_SLEEP;
            oa2_inn_CR1_REG &= oa2_inn_USBIO_CR1_OFF;
        #endif
    #endif
    #if defined(CYIPBLOCK_m0s8ioss_VERSION) && defined(oa2_inn__SIO)
        oa2_inn_backup.sioState = oa2_inn_SIO_REG;
        /* SIO requires unregulated output buffer and single ended input buffer */
        oa2_inn_SIO_REG &= (uint32)(~oa2_inn_SIO_LPM_MASK);
    #endif  
}


/*******************************************************************************
* Function Name: oa2_inn_Wakeup
****************************************************************************//**
*
* \brief Restores the pin configuration that was saved during Pin_Sleep().
*
* For USBIO pins, the wakeup is only triggered for falling edge interrupts.
*
* <b>Note</b> This function is available in PSoC 4 only.
*
* \return 
*  None
*  
* \funcusage
*  Refer to oa2_inn_Sleep() for an example usage.
*******************************************************************************/
void oa2_inn_Wakeup(void)
{
    #if defined(oa2_inn__PC)
        oa2_inn_PC = oa2_inn_backup.pcState;
    #else
        #if (CY_PSOC4_4200L)
            /* Restore the regulator state and come out of suspend mode */
            oa2_inn_USB_POWER_REG &= oa2_inn_USBIO_EXIT_SLEEP_PH1;
            oa2_inn_CR1_REG = oa2_inn_backup.usbState;
            oa2_inn_USB_POWER_REG &= oa2_inn_USBIO_EXIT_SLEEP_PH2;
        #endif
    #endif
    #if defined(CYIPBLOCK_m0s8ioss_VERSION) && defined(oa2_inn__SIO)
        oa2_inn_SIO_REG = oa2_inn_backup.sioState;
    #endif
}


/* [] END OF FILE */
