/*******************************************************************************
* File Name: oa2_inn.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_oa2_inn_ALIASES_H) /* Pins oa2_inn_ALIASES_H */
#define CY_PINS_oa2_inn_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define oa2_inn_0			(oa2_inn__0__PC)
#define oa2_inn_0_PS		(oa2_inn__0__PS)
#define oa2_inn_0_PC		(oa2_inn__0__PC)
#define oa2_inn_0_DR		(oa2_inn__0__DR)
#define oa2_inn_0_SHIFT	(oa2_inn__0__SHIFT)
#define oa2_inn_0_INTR	((uint16)((uint16)0x0003u << (oa2_inn__0__SHIFT*2u)))

#define oa2_inn_INTR_ALL	 ((uint16)(oa2_inn_0_INTR))


#endif /* End Pins oa2_inn_ALIASES_H */


/* [] END OF FILE */
