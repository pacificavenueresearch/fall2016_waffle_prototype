/*******************************************************************************
* File Name: OpampFilter2.h
* Version 1.20
*
* Description:
*  This file contains the function prototypes and constants used in
*  the Opamp (Analog Buffer) Component.
*
*
********************************************************************************
* Copyright 2013-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/


#if !defined(CY_OPAMP_OpampFilter2_H)
#define CY_OPAMP_OpampFilter2_H

#include "cytypes.h"
#include "cyfitter.h"


/***************************************
*       Type Definitions
***************************************/

/* Structure to save state before go to sleep */
typedef struct
{
    uint8  enableState;
} OpampFilter2_BACKUP_STRUCT;


/**************************************
*        Function Prototypes
**************************************/
void OpampFilter2_Init(void);
void OpampFilter2_Enable(void);
void OpampFilter2_Start(void);
void OpampFilter2_Stop(void);
void OpampFilter2_SetPower(uint32 power);
void OpampFilter2_PumpControl(uint32 onOff);
void OpampFilter2_Sleep(void);
void OpampFilter2_Wakeup(void);
void OpampFilter2_SaveConfig(void);
void OpampFilter2_RestoreConfig(void);


/**************************************
*           API Constants
**************************************/

/* Parameters for SetPower() function */
#define OpampFilter2_LOW_POWER      (1u)
#define OpampFilter2_MED_POWER      (2u)
#define OpampFilter2_HIGH_POWER     (3u)


/* Parameters for PumpControl() function */
#define OpampFilter2_PUMP_ON        (1u)
#define OpampFilter2_PUMP_OFF       (0u)


/***************************************
*   Initial Parameter Constants
****************************************/

#define OpampFilter2_OUTPUT_CURRENT         (1u)
#define OpampFilter2_POWER                  (2u)
#define OpampFilter2_MODE                   (0u)
#define OpampFilter2_OA_COMP_TRIM_VALUE     (2u)
#define OpampFilter2_DEEPSLEEP_SUPPORT      (0u)


/***************************************
*    Variables with External Linkage
***************************************/

extern uint8  OpampFilter2_initVar;


/**************************************
*             Registers
**************************************/

#ifdef CYIPBLOCK_m0s8pass4b_VERSION
    #define OpampFilter2_CTB_CTRL_REG       (*(reg32 *) OpampFilter2_cy_psoc4_abuf__CTB_CTB_CTRL)
    #define OpampFilter2_CTB_CTRL_PTR       ( (reg32 *) OpampFilter2_cy_psoc4_abuf__CTB_CTB_CTRL)
#else
    #define OpampFilter2_CTB_CTRL_REG       (*(reg32 *) OpampFilter2_cy_psoc4_abuf__CTBM_CTB_CTRL)
    #define OpampFilter2_CTB_CTRL_PTR       ( (reg32 *) OpampFilter2_cy_psoc4_abuf__CTBM_CTB_CTRL)
#endif /* CYIPBLOCK_m0s8pass4b_VERSION */

#define OpampFilter2_OA_RES_CTRL_REG    (*(reg32 *) OpampFilter2_cy_psoc4_abuf__OA_RES_CTRL)
#define OpampFilter2_OA_RES_CTRL_PTR    ( (reg32 *) OpampFilter2_cy_psoc4_abuf__OA_RES_CTRL)
#define OpampFilter2_OA_COMP_TRIM_REG   (*(reg32 *) OpampFilter2_cy_psoc4_abuf__OA_COMP_TRIM)
#define OpampFilter2_OA_COMP_TRIM_PTR   ( (reg32 *) OpampFilter2_cy_psoc4_abuf__OA_COMP_TRIM)


/***************************************
*        Registers Constants
***************************************/

/* OpampFilter2_CTB_CTRL_REG */
#define OpampFilter2_CTB_CTRL_DEEPSLEEP_ON_SHIFT    (30u)   /* [30] Selects behavior CTB IP in the DeepSleep power mode */
#define OpampFilter2_CTB_CTRL_ENABLED_SHIFT         (31u)   /* [31] Enable of the CTB IP */


#define OpampFilter2_CTB_CTRL_DEEPSLEEP_ON          ((uint32) 0x01u << OpampFilter2_CTB_CTRL_DEEPSLEEP_ON_SHIFT)
#define OpampFilter2_CTB_CTRL_ENABLED               ((uint32) 0x01u << OpampFilter2_CTB_CTRL_ENABLED_SHIFT)


/* OpampFilter2_OA_RES_CTRL_REG */
#define OpampFilter2_OA_PWR_MODE_SHIFT          (0u)    /* [1:0]    Power level */
#define OpampFilter2_OA_DRIVE_STR_SEL_SHIFT     (2u)    /* [2]      Opamp output strenght select: 0 - 1x, 1 - 10x */
#define OpampFilter2_OA_COMP_EN_SHIFT           (4u)    /* [4]      CTB IP mode: 0 - Opamp, 1 - Comparator  */
#define OpampFilter2_OA_PUMP_EN_SHIFT           (11u)   /* [11]     Pump enable */


#define OpampFilter2_OA_PWR_MODE                ((uint32) 0x02u << OpampFilter2_OA_PWR_MODE_SHIFT)
#define OpampFilter2_OA_PWR_MODE_MASK           ((uint32) 0x03u << OpampFilter2_OA_PWR_MODE_SHIFT)
#define OpampFilter2_OA_DRIVE_STR_SEL_1X        ((uint32) 0x00u << OpampFilter2_OA_DRIVE_STR_SEL_SHIFT)
#define OpampFilter2_OA_DRIVE_STR_SEL_10X       ((uint32) 0x01u << OpampFilter2_OA_DRIVE_STR_SEL_SHIFT)
#define OpampFilter2_OA_DRIVE_STR_SEL_MASK      ((uint32) 0x01u << OpampFilter2_OA_DRIVE_STR_SEL_SHIFT)
#define OpampFilter2_OA_COMP_EN                 ((uint32) 0x00u << OpampFilter2_OA_COMP_EN_SHIFT)
#define OpampFilter2_OA_PUMP_EN                 ((uint32) 0x01u << OpampFilter2_OA_PUMP_EN_SHIFT)


/***************************************
*       Init Macros Definitions
***************************************/

#define OpampFilter2_GET_DEEPSLEEP_ON(deepSleep)    ((0u != (deepSleep)) ? (OpampFilter2_CTB_CTRL_DEEPSLEEP_ON) : (0u))
#define OpampFilter2_GET_OA_DRIVE_STR(current)      ((0u != (current)) ? (OpampFilter2_OA_DRIVE_STR_SEL_10X) : \
                                                                             (OpampFilter2_OA_DRIVE_STR_SEL_1X))
#define OpampFilter2_GET_OA_PWR_MODE(mode)          ((mode) & OpampFilter2_OA_PWR_MODE_MASK)
#define OpampFilter2_CHECK_PWR_MODE_OFF             (0u != (OpampFilter2_OA_RES_CTRL_REG & \
                                                                OpampFilter2_OA_PWR_MODE_MASK))

/* Returns true if component available in Deep Sleep power mode*/ 
#define OpampFilter2_CHECK_DEEPSLEEP_SUPPORT        (0u != OpampFilter2_DEEPSLEEP_SUPPORT) 

#define OpampFilter2_DEFAULT_CTB_CTRL (OpampFilter2_GET_DEEPSLEEP_ON(OpampFilter2_DEEPSLEEP_SUPPORT) | \
                                           OpampFilter2_CTB_CTRL_ENABLED)

#define OpampFilter2_DEFAULT_OA_RES_CTRL (OpampFilter2_OA_COMP_EN | \
                                              OpampFilter2_GET_OA_DRIVE_STR(OpampFilter2_OUTPUT_CURRENT))

#define OpampFilter2_DEFAULT_OA_COMP_TRIM_REG (OpampFilter2_OA_COMP_TRIM_VALUE)


/***************************************
* The following code is DEPRECATED and 
* should not be used in new projects.
***************************************/

#define OpampFilter2_LOWPOWER                   (OpampFilter2_LOW_POWER)
#define OpampFilter2_MEDPOWER                   (OpampFilter2_MED_POWER)
#define OpampFilter2_HIGHPOWER                  (OpampFilter2_HIGH_POWER)

/* PUMP ON/OFF defines */
#define OpampFilter2_PUMPON                     (OpampFilter2_PUMP_ON)
#define OpampFilter2_PUMPOFF                    (OpampFilter2_PUMP_OFF)

#define OpampFilter2_OA_CTRL                    (OpampFilter2_CTB_CTRL_REG)
#define OpampFilter2_OA_RES_CTRL                (OpampFilter2_OA_RES_CTRL_REG)

/* Bit Field  OA_CTRL */
#define OpampFilter2_OA_CTB_EN_SHIFT            (OpampFilter2_CTB_CTRL_ENABLED_SHIFT)
#define OpampFilter2_OA_PUMP_CTRL_SHIFT         (OpampFilter2_OA_PUMP_EN_SHIFT)
#define OpampFilter2_OA_PUMP_EN_MASK            (0x800u)
#define OpampFilter2_PUMP_PROTECT_MASK          (1u)


#endif    /* CY_OPAMP_OpampFilter2_H */


/* [] END OF FILE */
