-- ======================================================================
-- PSOC4_Waffle_1b.ctl generated from PSOC4_Waffle_1b
-- 09/05/2016 at 16:36
-- This file is auto generated. ANY EDITS YOU MAKE MAY BE LOST WHEN THIS FILE IS REGENERATED!!!
-- ======================================================================

-- TopDesign
-- =============================================================================
-- The following directives assign pins to the locations specific for the
-- CY8CKIT-042-BLE kit.
-- =============================================================================

-- === UART ===
attribute port_location of \UART:rx(0)\ : label is "PORT(1,4)";
attribute port_location of \UART:tx(0)\ : label is "PORT(1,5)";

-- === I2C ===
attribute port_location of \I2C:scl(0)\ : label is "PORT(3,5)";
attribute port_location of \I2C:sda(0)\ : label is "PORT(3,4)";

-- === RGB LED ===
attribute port_location of RED(0) : label is "PORT(2,6)"; -- RED LED
attribute port_location of GREEN(0) : label is "PORT(3,6)"; -- GREEN LED
attribute port_location of BLUE(0) : label is "PORT(3,7)"; -- BLUE LED

-- === ADC CH0 ===
attribute port_location of V1(0) : label is "PORT(2,0)";

-- === USER SWITCH ===
attribute port_location of REPLACE_WITH_ACTUAL_PIN_NAME(0) : label is "PORT(2,7)";
-- M0S8 Clock Editor
-- Directives Editor
-- Analog Device Editor
