/*******************************************************************************
* File Name: OpampLPFilter.h
* Version 1.20
*
* Description:
*  This file contains the function prototypes and constants used in
*  the Opamp (Analog Buffer) Component.
*
*
********************************************************************************
* Copyright 2013-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/


#if !defined(CY_OPAMP_OpampLPFilter_H)
#define CY_OPAMP_OpampLPFilter_H

#include "cytypes.h"
#include "cyfitter.h"


/***************************************
*       Type Definitions
***************************************/

/* Structure to save state before go to sleep */
typedef struct
{
    uint8  enableState;
} OpampLPFilter_BACKUP_STRUCT;


/**************************************
*        Function Prototypes
**************************************/
void OpampLPFilter_Init(void);
void OpampLPFilter_Enable(void);
void OpampLPFilter_Start(void);
void OpampLPFilter_Stop(void);
void OpampLPFilter_SetPower(uint32 power);
void OpampLPFilter_PumpControl(uint32 onOff);
void OpampLPFilter_Sleep(void);
void OpampLPFilter_Wakeup(void);
void OpampLPFilter_SaveConfig(void);
void OpampLPFilter_RestoreConfig(void);


/**************************************
*           API Constants
**************************************/

/* Parameters for SetPower() function */
#define OpampLPFilter_LOW_POWER      (1u)
#define OpampLPFilter_MED_POWER      (2u)
#define OpampLPFilter_HIGH_POWER     (3u)


/* Parameters for PumpControl() function */
#define OpampLPFilter_PUMP_ON        (1u)
#define OpampLPFilter_PUMP_OFF       (0u)


/***************************************
*   Initial Parameter Constants
****************************************/

#define OpampLPFilter_OUTPUT_CURRENT         (1u)
#define OpampLPFilter_POWER                  (2u)
#define OpampLPFilter_MODE                   (0u)
#define OpampLPFilter_OA_COMP_TRIM_VALUE     (3u)
#define OpampLPFilter_DEEPSLEEP_SUPPORT      (0u)


/***************************************
*    Variables with External Linkage
***************************************/

extern uint8  OpampLPFilter_initVar;


/**************************************
*             Registers
**************************************/

#ifdef CYIPBLOCK_m0s8pass4b_VERSION
    #define OpampLPFilter_CTB_CTRL_REG       (*(reg32 *) OpampLPFilter_cy_psoc4_abuf__CTB_CTB_CTRL)
    #define OpampLPFilter_CTB_CTRL_PTR       ( (reg32 *) OpampLPFilter_cy_psoc4_abuf__CTB_CTB_CTRL)
#else
    #define OpampLPFilter_CTB_CTRL_REG       (*(reg32 *) OpampLPFilter_cy_psoc4_abuf__CTBM_CTB_CTRL)
    #define OpampLPFilter_CTB_CTRL_PTR       ( (reg32 *) OpampLPFilter_cy_psoc4_abuf__CTBM_CTB_CTRL)
#endif /* CYIPBLOCK_m0s8pass4b_VERSION */

#define OpampLPFilter_OA_RES_CTRL_REG    (*(reg32 *) OpampLPFilter_cy_psoc4_abuf__OA_RES_CTRL)
#define OpampLPFilter_OA_RES_CTRL_PTR    ( (reg32 *) OpampLPFilter_cy_psoc4_abuf__OA_RES_CTRL)
#define OpampLPFilter_OA_COMP_TRIM_REG   (*(reg32 *) OpampLPFilter_cy_psoc4_abuf__OA_COMP_TRIM)
#define OpampLPFilter_OA_COMP_TRIM_PTR   ( (reg32 *) OpampLPFilter_cy_psoc4_abuf__OA_COMP_TRIM)


/***************************************
*        Registers Constants
***************************************/

/* OpampLPFilter_CTB_CTRL_REG */
#define OpampLPFilter_CTB_CTRL_DEEPSLEEP_ON_SHIFT    (30u)   /* [30] Selects behavior CTB IP in the DeepSleep power mode */
#define OpampLPFilter_CTB_CTRL_ENABLED_SHIFT         (31u)   /* [31] Enable of the CTB IP */


#define OpampLPFilter_CTB_CTRL_DEEPSLEEP_ON          ((uint32) 0x01u << OpampLPFilter_CTB_CTRL_DEEPSLEEP_ON_SHIFT)
#define OpampLPFilter_CTB_CTRL_ENABLED               ((uint32) 0x01u << OpampLPFilter_CTB_CTRL_ENABLED_SHIFT)


/* OpampLPFilter_OA_RES_CTRL_REG */
#define OpampLPFilter_OA_PWR_MODE_SHIFT          (0u)    /* [1:0]    Power level */
#define OpampLPFilter_OA_DRIVE_STR_SEL_SHIFT     (2u)    /* [2]      Opamp output strenght select: 0 - 1x, 1 - 10x */
#define OpampLPFilter_OA_COMP_EN_SHIFT           (4u)    /* [4]      CTB IP mode: 0 - Opamp, 1 - Comparator  */
#define OpampLPFilter_OA_PUMP_EN_SHIFT           (11u)   /* [11]     Pump enable */


#define OpampLPFilter_OA_PWR_MODE                ((uint32) 0x02u << OpampLPFilter_OA_PWR_MODE_SHIFT)
#define OpampLPFilter_OA_PWR_MODE_MASK           ((uint32) 0x03u << OpampLPFilter_OA_PWR_MODE_SHIFT)
#define OpampLPFilter_OA_DRIVE_STR_SEL_1X        ((uint32) 0x00u << OpampLPFilter_OA_DRIVE_STR_SEL_SHIFT)
#define OpampLPFilter_OA_DRIVE_STR_SEL_10X       ((uint32) 0x01u << OpampLPFilter_OA_DRIVE_STR_SEL_SHIFT)
#define OpampLPFilter_OA_DRIVE_STR_SEL_MASK      ((uint32) 0x01u << OpampLPFilter_OA_DRIVE_STR_SEL_SHIFT)
#define OpampLPFilter_OA_COMP_EN                 ((uint32) 0x00u << OpampLPFilter_OA_COMP_EN_SHIFT)
#define OpampLPFilter_OA_PUMP_EN                 ((uint32) 0x01u << OpampLPFilter_OA_PUMP_EN_SHIFT)


/***************************************
*       Init Macros Definitions
***************************************/

#define OpampLPFilter_GET_DEEPSLEEP_ON(deepSleep)    ((0u != (deepSleep)) ? (OpampLPFilter_CTB_CTRL_DEEPSLEEP_ON) : (0u))
#define OpampLPFilter_GET_OA_DRIVE_STR(current)      ((0u != (current)) ? (OpampLPFilter_OA_DRIVE_STR_SEL_10X) : \
                                                                             (OpampLPFilter_OA_DRIVE_STR_SEL_1X))
#define OpampLPFilter_GET_OA_PWR_MODE(mode)          ((mode) & OpampLPFilter_OA_PWR_MODE_MASK)
#define OpampLPFilter_CHECK_PWR_MODE_OFF             (0u != (OpampLPFilter_OA_RES_CTRL_REG & \
                                                                OpampLPFilter_OA_PWR_MODE_MASK))

/* Returns true if component available in Deep Sleep power mode*/ 
#define OpampLPFilter_CHECK_DEEPSLEEP_SUPPORT        (0u != OpampLPFilter_DEEPSLEEP_SUPPORT) 

#define OpampLPFilter_DEFAULT_CTB_CTRL (OpampLPFilter_GET_DEEPSLEEP_ON(OpampLPFilter_DEEPSLEEP_SUPPORT) | \
                                           OpampLPFilter_CTB_CTRL_ENABLED)

#define OpampLPFilter_DEFAULT_OA_RES_CTRL (OpampLPFilter_OA_COMP_EN | \
                                              OpampLPFilter_GET_OA_DRIVE_STR(OpampLPFilter_OUTPUT_CURRENT))

#define OpampLPFilter_DEFAULT_OA_COMP_TRIM_REG (OpampLPFilter_OA_COMP_TRIM_VALUE)


/***************************************
* The following code is DEPRECATED and 
* should not be used in new projects.
***************************************/

#define OpampLPFilter_LOWPOWER                   (OpampLPFilter_LOW_POWER)
#define OpampLPFilter_MEDPOWER                   (OpampLPFilter_MED_POWER)
#define OpampLPFilter_HIGHPOWER                  (OpampLPFilter_HIGH_POWER)

/* PUMP ON/OFF defines */
#define OpampLPFilter_PUMPON                     (OpampLPFilter_PUMP_ON)
#define OpampLPFilter_PUMPOFF                    (OpampLPFilter_PUMP_OFF)

#define OpampLPFilter_OA_CTRL                    (OpampLPFilter_CTB_CTRL_REG)
#define OpampLPFilter_OA_RES_CTRL                (OpampLPFilter_OA_RES_CTRL_REG)

/* Bit Field  OA_CTRL */
#define OpampLPFilter_OA_CTB_EN_SHIFT            (OpampLPFilter_CTB_CTRL_ENABLED_SHIFT)
#define OpampLPFilter_OA_PUMP_CTRL_SHIFT         (OpampLPFilter_OA_PUMP_EN_SHIFT)
#define OpampLPFilter_OA_PUMP_EN_MASK            (0x800u)
#define OpampLPFilter_PUMP_PROTECT_MASK          (1u)


#endif    /* CY_OPAMP_OpampLPFilter_H */


/* [] END OF FILE */
