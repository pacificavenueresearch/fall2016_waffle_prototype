/*******************************************************************************
* File Name: oa_inn.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_oa_inn_H) /* Pins oa_inn_H */
#define CY_PINS_oa_inn_H

#include "cytypes.h"
#include "cyfitter.h"
#include "oa_inn_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} oa_inn_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   oa_inn_Read(void);
void    oa_inn_Write(uint8 value);
uint8   oa_inn_ReadDataReg(void);
#if defined(oa_inn__PC) || (CY_PSOC4_4200L) 
    void    oa_inn_SetDriveMode(uint8 mode);
#endif
void    oa_inn_SetInterruptMode(uint16 position, uint16 mode);
uint8   oa_inn_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void oa_inn_Sleep(void); 
void oa_inn_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(oa_inn__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define oa_inn_DRIVE_MODE_BITS        (3)
    #define oa_inn_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - oa_inn_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the oa_inn_SetDriveMode() function.
         *  @{
         */
        #define oa_inn_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define oa_inn_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define oa_inn_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define oa_inn_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define oa_inn_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define oa_inn_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define oa_inn_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define oa_inn_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define oa_inn_MASK               oa_inn__MASK
#define oa_inn_SHIFT              oa_inn__SHIFT
#define oa_inn_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in oa_inn_SetInterruptMode() function.
     *  @{
     */
        #define oa_inn_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define oa_inn_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define oa_inn_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define oa_inn_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(oa_inn__SIO)
    #define oa_inn_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(oa_inn__PC) && (CY_PSOC4_4200L)
    #define oa_inn_USBIO_ENABLE               ((uint32)0x80000000u)
    #define oa_inn_USBIO_DISABLE              ((uint32)(~oa_inn_USBIO_ENABLE))
    #define oa_inn_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define oa_inn_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define oa_inn_USBIO_ENTER_SLEEP          ((uint32)((1u << oa_inn_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << oa_inn_USBIO_SUSPEND_DEL_SHIFT)))
    #define oa_inn_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << oa_inn_USBIO_SUSPEND_SHIFT)))
    #define oa_inn_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << oa_inn_USBIO_SUSPEND_DEL_SHIFT)))
    #define oa_inn_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(oa_inn__PC)
    /* Port Configuration */
    #define oa_inn_PC                 (* (reg32 *) oa_inn__PC)
#endif
/* Pin State */
#define oa_inn_PS                     (* (reg32 *) oa_inn__PS)
/* Data Register */
#define oa_inn_DR                     (* (reg32 *) oa_inn__DR)
/* Input Buffer Disable Override */
#define oa_inn_INP_DIS                (* (reg32 *) oa_inn__PC2)

/* Interrupt configuration Registers */
#define oa_inn_INTCFG                 (* (reg32 *) oa_inn__INTCFG)
#define oa_inn_INTSTAT                (* (reg32 *) oa_inn__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define oa_inn_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(oa_inn__SIO)
    #define oa_inn_SIO_REG            (* (reg32 *) oa_inn__SIO)
#endif /* (oa_inn__SIO_CFG) */

/* USBIO registers */
#if !defined(oa_inn__PC) && (CY_PSOC4_4200L)
    #define oa_inn_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define oa_inn_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define oa_inn_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define oa_inn_DRIVE_MODE_SHIFT       (0x00u)
#define oa_inn_DRIVE_MODE_MASK        (0x07u << oa_inn_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins oa_inn_H */


/* [] END OF FILE */
