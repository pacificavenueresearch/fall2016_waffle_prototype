/*******************************************************************************
* File Name: oa_inp.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_oa_inp_ALIASES_H) /* Pins oa_inp_ALIASES_H */
#define CY_PINS_oa_inp_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define oa_inp_0			(oa_inp__0__PC)
#define oa_inp_0_PS		(oa_inp__0__PS)
#define oa_inp_0_PC		(oa_inp__0__PC)
#define oa_inp_0_DR		(oa_inp__0__DR)
#define oa_inp_0_SHIFT	(oa_inp__0__SHIFT)
#define oa_inp_0_INTR	((uint16)((uint16)0x0003u << (oa_inp__0__SHIFT*2u)))

#define oa_inp_INTR_ALL	 ((uint16)(oa_inp_0_INTR))


#endif /* End Pins oa_inp_ALIASES_H */


/* [] END OF FILE */
