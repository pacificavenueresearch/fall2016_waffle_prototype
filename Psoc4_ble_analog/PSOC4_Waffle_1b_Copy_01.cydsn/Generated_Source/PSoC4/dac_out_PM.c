/*******************************************************************************
* File Name: dac_out.c  
* Version 2.20
*
* Description:
*  This file contains APIs to set up the Pins component for low power modes.
*
* Note:
*
********************************************************************************
* Copyright 2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "dac_out.h"

static dac_out_BACKUP_STRUCT  dac_out_backup = {0u, 0u, 0u};


/*******************************************************************************
* Function Name: dac_out_Sleep
****************************************************************************//**
*
* \brief Stores the pin configuration and prepares the pin for entering chip 
*  deep-sleep/hibernate modes. This function must be called for SIO and USBIO
*  pins. It is not essential if using GPIO or GPIO_OVT pins.
*
* <b>Note</b> This function is available in PSoC 4 only.
*
* \return 
*  None 
*  
* \sideeffect
*  For SIO pins, this function configures the pin input threshold to CMOS and
*  drive level to Vddio. This is needed for SIO pins when in device 
*  deep-sleep/hibernate modes.
*
* \funcusage
*  \snippet dac_out_SUT.c usage_dac_out_Sleep_Wakeup
*******************************************************************************/
void dac_out_Sleep(void)
{
    #if defined(dac_out__PC)
        dac_out_backup.pcState = dac_out_PC;
    #else
        #if (CY_PSOC4_4200L)
            /* Save the regulator state and put the PHY into suspend mode */
            dac_out_backup.usbState = dac_out_CR1_REG;
            dac_out_USB_POWER_REG |= dac_out_USBIO_ENTER_SLEEP;
            dac_out_CR1_REG &= dac_out_USBIO_CR1_OFF;
        #endif
    #endif
    #if defined(CYIPBLOCK_m0s8ioss_VERSION) && defined(dac_out__SIO)
        dac_out_backup.sioState = dac_out_SIO_REG;
        /* SIO requires unregulated output buffer and single ended input buffer */
        dac_out_SIO_REG &= (uint32)(~dac_out_SIO_LPM_MASK);
    #endif  
}


/*******************************************************************************
* Function Name: dac_out_Wakeup
****************************************************************************//**
*
* \brief Restores the pin configuration that was saved during Pin_Sleep().
*
* For USBIO pins, the wakeup is only triggered for falling edge interrupts.
*
* <b>Note</b> This function is available in PSoC 4 only.
*
* \return 
*  None
*  
* \funcusage
*  Refer to dac_out_Sleep() for an example usage.
*******************************************************************************/
void dac_out_Wakeup(void)
{
    #if defined(dac_out__PC)
        dac_out_PC = dac_out_backup.pcState;
    #else
        #if (CY_PSOC4_4200L)
            /* Restore the regulator state and come out of suspend mode */
            dac_out_USB_POWER_REG &= dac_out_USBIO_EXIT_SLEEP_PH1;
            dac_out_CR1_REG = dac_out_backup.usbState;
            dac_out_USB_POWER_REG &= dac_out_USBIO_EXIT_SLEEP_PH2;
        #endif
    #endif
    #if defined(CYIPBLOCK_m0s8ioss_VERSION) && defined(dac_out__SIO)
        dac_out_SIO_REG = dac_out_backup.sioState;
    #endif
}


/* [] END OF FILE */
