/*******************************************************************************
* File Name: OpampLPFilter.c
* Version 1.20
*
* Description:
*  This file provides the source code to the API for the Opamp (Analog Buffer)
*  Component.
*
*
********************************************************************************
* Copyright 2013-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "OpampLPFilter.h"

uint8 OpampLPFilter_initVar = 0u; /* Defines if component was initialized */
static uint32 OpampLPFilter_internalPower = 0u; /* Defines component Power value */


/*******************************************************************************
* Function Name: OpampLPFilter_Init
********************************************************************************
*
* Summary:
*  Initializes or restores the component according to the customizer Configure 
*  dialog settings. It is not necessary to call Init() because the Start() API 
*  calls this function and is the preferred method to begin the component operation.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void OpampLPFilter_Init(void)
{
    OpampLPFilter_internalPower = OpampLPFilter_POWER;
    OpampLPFilter_CTB_CTRL_REG = OpampLPFilter_DEFAULT_CTB_CTRL;
    OpampLPFilter_OA_RES_CTRL_REG = OpampLPFilter_DEFAULT_OA_RES_CTRL;
    OpampLPFilter_OA_COMP_TRIM_REG = OpampLPFilter_DEFAULT_OA_COMP_TRIM_REG;
}


/*******************************************************************************
* Function Name: OpampLPFilter_Enable
********************************************************************************
*
* Summary:
*  Activates the hardware and begins the component operation. It is not necessary to 
*  call Enable() because the Start() API calls this function, which is the 
*  preferred method to begin the component operation.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void OpampLPFilter_Enable(void)
{
    OpampLPFilter_OA_RES_CTRL_REG |= OpampLPFilter_internalPower | \
                                        OpampLPFilter_OA_PUMP_EN;
}


/*******************************************************************************
* Function Name: OpampLPFilter_Start
********************************************************************************
*
* Summary:
*  Performs all of the required initialization for the component and enables power 
*  to the block. The first time the routine is executed, the Power level, Mode, 
*  and Output mode are set. When called to restart the Opamp following a Stop() call, 
*  the current component parameter settings are retained.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  OpampLPFilter_initVar: Used to check the initial configuration, modified
*  when this function is called for the first time.
*
*******************************************************************************/
void OpampLPFilter_Start(void)
{
    if( 0u == OpampLPFilter_initVar)
    {
        OpampLPFilter_Init();
        OpampLPFilter_initVar = 1u;
    }
    OpampLPFilter_Enable();
}


/*******************************************************************************
* Function Name: OpampLPFilter_Stop
********************************************************************************
*
* Summary:
*  Turn off the Opamp block.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void OpampLPFilter_Stop(void)
{
    OpampLPFilter_OA_RES_CTRL_REG &= ((uint32)~(OpampLPFilter_OA_PWR_MODE_MASK | \
                                                   OpampLPFilter_OA_PUMP_EN));
}


/*******************************************************************************
* Function Name: OpampLPFilter_SetPower
********************************************************************************
*
* Summary:
*  Sets the Opamp to one of the three power levels.
*
* Parameters:
*  power: power levels.
*   OpampLPFilter_LOW_POWER - Lowest active power
*   OpampLPFilter_MED_POWER - Medium power
*   OpampLPFilter_HIGH_POWER - Highest active power
*
* Return:
*  None
*
**********************************************************************************/
void OpampLPFilter_SetPower(uint32 power)
{
    uint32 tmp;
    
    OpampLPFilter_internalPower = OpampLPFilter_GET_OA_PWR_MODE(power);
    tmp = OpampLPFilter_OA_RES_CTRL_REG & \
           (uint32)~OpampLPFilter_OA_PWR_MODE_MASK;
    OpampLPFilter_OA_RES_CTRL_REG = tmp | OpampLPFilter_internalPower;
}


/*******************************************************************************
* Function Name: OpampLPFilter_PumpControl
********************************************************************************
*
* Summary:
*  Allows the user to turn the Opamp's boost pump on or off. By Default the Start() 
*  function turns on the pump. Use this API to turn it off. The boost must be 
*  turned on when the supply is less than 2.7 volts and off if the supply is more 
*  than 4 volts.
*
* Parameters:
*  onOff: Control the pump.
*   OpampLPFilter_PUMP_OFF - Turn off the pump
*   OpampLPFilter_PUMP_ON - Turn on the pump
*
* Return:
*  None
*
**********************************************************************************/
void OpampLPFilter_PumpControl(uint32 onOff)
{
    
    if(0u != onOff)
    {
        OpampLPFilter_OA_RES_CTRL |= OpampLPFilter_OA_PUMP_EN;    
    }
    else
    {
        OpampLPFilter_OA_RES_CTRL &= (uint32)~OpampLPFilter_OA_PUMP_EN;
    }
}


/* [] END OF FILE */
