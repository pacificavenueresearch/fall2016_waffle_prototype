/*******************************************************************************
* File Name: main.c
*
* Version:   1.0
*
* Description:
*  This example shows how to use the DMA to transfer data from a RAM array to
*  the SPI TX buffer, and shows how to use the DMA to transfer data from the SPI
*  RX buffer to a RAM array.
*
********************************************************************************
* Copyright 2015, Cypress Semiconductor Corporation. All rights reserved.
* This software is owned by Cypress Semiconductor Corporation and is protected
* by and subject to worldwide patent and copyright laws and treaties.
* Therefore, you may use this software only as provided in the license agreement
* accompanying the software package from which you obtained this software.
* CYPRESS AND ITS SUPPLIERS MAKE NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
* WITH REGARD TO THIS SOFTWARE, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT,
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
*******************************************************************************/

#include <project.h>
#include <string.h>

//**** nrF24L0
#include <nrf24l01_regmap.h>
//**** nrF24L0

#define     DESCR0              0
#define     DESCR1              1
#define     TX_BUFFER_SIZE      1
#define     RX_BUFFER_SIZE      2

/* LED control defines. LED is active low. */
#define     LED_ON              0
#define     LED_OFF             1

/* Data buffers */
static const int8 CYCODE masterTxBuffer[TX_BUFFER_SIZE] = {CONFIG};
static int8 masterRxBuffer[RX_BUFFER_SIZE] = {0};

/* loop counter */
uint8 ccount = 0;


/* not doing slave here
static const int8 CYCODE slaveTxBuffer[BUFFER_SIZE]  = {"Data to return by SPI Slave !!!!"};
static int8 slaveRxBuffer[BUFFER_SIZE] = {0};
*/

/*******************************************************************************
* Function Name: main
********************************************************************************
*
* Summary:
*  This function starts the SPI communication, configures and starts DMA
*  transfers. Once data exchange completes, checks the content of sent and
*  received data.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
int main()
{
    /* Start TxDmaS to fill SPIS transmit FIFO in preparation for a data
    * transfer. Other DMA channels are disabled.
    */
  //   TxDmaS_Start((void *)slaveTxBuffer, (void *)SPIS_TXDATA_PTR);
    
    /* Start SPI communication. */
    
     
    SPIM_Start();

    /* When configured in CPHA=0 mode, the SPI slave sends the very first byte
    * from the internal shift register and then loads the next data bytes from
    * transmit FIFO. Therefore, the first byte should either be written directly
    * into the shift register or discarded by SPI master.
    * Send a dummy byte not using a DMA. This byte will be discarded.
    */
    SPIM_SpiUartWriteTxData(0u);

    while(0u == SPIM_SpiUartGetRxBufferSize())
    {
        /* Wait for the first byte exchange. */
    }

    /* The first received byte is discarded. Clear data buffers. */
    SPIM_SpiUartClearRxBuffer();
 

    /* Start other DMA channels to begin data transfer. */
    RxDmaM_Start((void *)SPIM_RX_FIFO_RD_PTR, (void *)masterRxBuffer);
    TxDmaM_Start((void *)masterTxBuffer, (void *)SPIM_TX_FIFO_WR_PTR);

    for(ccount=1;10;ccount++)
    {
        /* Check whether data exchange has been finished. RxDmaM and RxDmaS are 
        * configured to set an interrupt when they finish transferring all data
        * elements.
        */
        
        if(0u == (CyDmaGetInterruptSourceMasked() ^ (RxDmaM_CHANNEL_MASK )))
        {
            /* Once asserted, interrupt bits remain high until cleared. */
            CyDmaClearInterruptSource(RxDmaM_CHANNEL_MASK );

            /* Clear previous transfer complete status. */
            SUCCESS_Write(LED_OFF);

            /* Compare content of receive and transmit buffers to check if
            * transfer has been successfully finished.
            */

            /* consider comparing against expected values for power up status and putting in subroutine */

            /* Set transfer complete status. */
            SUCCESS_Write(LED_ON);

            /* Reset receive buffers. */
            memset((void *) masterRxBuffer, 0, RX_BUFFER_SIZE);
        }
            
            /* Re-enable transfer. TxDmaM controls the number of bytes to be sent
            * to the slave and correspondingly the number of bytes returned by the
            * slave. Therefore it is configured to be invalidated when it
            * finishes a transfer.
            */
            
            TxDmaM_ValidateDescriptor(DESCR0);
            TxDmaM_ChEnable();
        
    }
}

/* [] END OF FILE */
