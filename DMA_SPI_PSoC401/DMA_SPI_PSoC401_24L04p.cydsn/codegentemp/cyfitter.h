#ifndef INCLUDED_CYFITTER_H
#define INCLUDED_CYFITTER_H
#include "cydevice_trm.h"

/* SPIM_miso_m */
#define SPIM_miso_m__0__DR CYREG_GPIO_PRT0_DR
#define SPIM_miso_m__0__DR_CLR CYREG_GPIO_PRT0_DR_CLR
#define SPIM_miso_m__0__DR_INV CYREG_GPIO_PRT0_DR_INV
#define SPIM_miso_m__0__DR_SET CYREG_GPIO_PRT0_DR_SET
#define SPIM_miso_m__0__HSIOM CYREG_HSIOM_PORT_SEL0
#define SPIM_miso_m__0__HSIOM_GPIO 0u
#define SPIM_miso_m__0__HSIOM_I2C 14u
#define SPIM_miso_m__0__HSIOM_I2C_SCL 14u
#define SPIM_miso_m__0__HSIOM_MASK 0x000000F0u
#define SPIM_miso_m__0__HSIOM_SHIFT 4u
#define SPIM_miso_m__0__HSIOM_SPI 15u
#define SPIM_miso_m__0__HSIOM_SPI_MISO 15u
#define SPIM_miso_m__0__HSIOM_UART 9u
#define SPIM_miso_m__0__HSIOM_UART_TX 9u
#define SPIM_miso_m__0__INTCFG CYREG_GPIO_PRT0_INTR_CFG
#define SPIM_miso_m__0__INTR CYREG_GPIO_PRT0_INTR
#define SPIM_miso_m__0__INTR_CFG CYREG_GPIO_PRT0_INTR_CFG
#define SPIM_miso_m__0__INTSTAT CYREG_GPIO_PRT0_INTR
#define SPIM_miso_m__0__MASK 0x02u
#define SPIM_miso_m__0__PA__CFG0 CYREG_UDB_PA0_CFG0
#define SPIM_miso_m__0__PA__CFG1 CYREG_UDB_PA0_CFG1
#define SPIM_miso_m__0__PA__CFG10 CYREG_UDB_PA0_CFG10
#define SPIM_miso_m__0__PA__CFG11 CYREG_UDB_PA0_CFG11
#define SPIM_miso_m__0__PA__CFG12 CYREG_UDB_PA0_CFG12
#define SPIM_miso_m__0__PA__CFG13 CYREG_UDB_PA0_CFG13
#define SPIM_miso_m__0__PA__CFG14 CYREG_UDB_PA0_CFG14
#define SPIM_miso_m__0__PA__CFG2 CYREG_UDB_PA0_CFG2
#define SPIM_miso_m__0__PA__CFG3 CYREG_UDB_PA0_CFG3
#define SPIM_miso_m__0__PA__CFG4 CYREG_UDB_PA0_CFG4
#define SPIM_miso_m__0__PA__CFG5 CYREG_UDB_PA0_CFG5
#define SPIM_miso_m__0__PA__CFG6 CYREG_UDB_PA0_CFG6
#define SPIM_miso_m__0__PA__CFG7 CYREG_UDB_PA0_CFG7
#define SPIM_miso_m__0__PA__CFG8 CYREG_UDB_PA0_CFG8
#define SPIM_miso_m__0__PA__CFG9 CYREG_UDB_PA0_CFG9
#define SPIM_miso_m__0__PC CYREG_GPIO_PRT0_PC
#define SPIM_miso_m__0__PC2 CYREG_GPIO_PRT0_PC2
#define SPIM_miso_m__0__PORT 0u
#define SPIM_miso_m__0__PS CYREG_GPIO_PRT0_PS
#define SPIM_miso_m__0__SHIFT 1u
#define SPIM_miso_m__DR CYREG_GPIO_PRT0_DR
#define SPIM_miso_m__DR_CLR CYREG_GPIO_PRT0_DR_CLR
#define SPIM_miso_m__DR_INV CYREG_GPIO_PRT0_DR_INV
#define SPIM_miso_m__DR_SET CYREG_GPIO_PRT0_DR_SET
#define SPIM_miso_m__INTCFG CYREG_GPIO_PRT0_INTR_CFG
#define SPIM_miso_m__INTR CYREG_GPIO_PRT0_INTR
#define SPIM_miso_m__INTR_CFG CYREG_GPIO_PRT0_INTR_CFG
#define SPIM_miso_m__INTSTAT CYREG_GPIO_PRT0_INTR
#define SPIM_miso_m__MASK 0x02u
#define SPIM_miso_m__PA__CFG0 CYREG_UDB_PA0_CFG0
#define SPIM_miso_m__PA__CFG1 CYREG_UDB_PA0_CFG1
#define SPIM_miso_m__PA__CFG10 CYREG_UDB_PA0_CFG10
#define SPIM_miso_m__PA__CFG11 CYREG_UDB_PA0_CFG11
#define SPIM_miso_m__PA__CFG12 CYREG_UDB_PA0_CFG12
#define SPIM_miso_m__PA__CFG13 CYREG_UDB_PA0_CFG13
#define SPIM_miso_m__PA__CFG14 CYREG_UDB_PA0_CFG14
#define SPIM_miso_m__PA__CFG2 CYREG_UDB_PA0_CFG2
#define SPIM_miso_m__PA__CFG3 CYREG_UDB_PA0_CFG3
#define SPIM_miso_m__PA__CFG4 CYREG_UDB_PA0_CFG4
#define SPIM_miso_m__PA__CFG5 CYREG_UDB_PA0_CFG5
#define SPIM_miso_m__PA__CFG6 CYREG_UDB_PA0_CFG6
#define SPIM_miso_m__PA__CFG7 CYREG_UDB_PA0_CFG7
#define SPIM_miso_m__PA__CFG8 CYREG_UDB_PA0_CFG8
#define SPIM_miso_m__PA__CFG9 CYREG_UDB_PA0_CFG9
#define SPIM_miso_m__PC CYREG_GPIO_PRT0_PC
#define SPIM_miso_m__PC2 CYREG_GPIO_PRT0_PC2
#define SPIM_miso_m__PORT 0u
#define SPIM_miso_m__PS CYREG_GPIO_PRT0_PS
#define SPIM_miso_m__SHIFT 1u

/* SPIM_mosi_m */
#define SPIM_mosi_m__0__DR CYREG_GPIO_PRT0_DR
#define SPIM_mosi_m__0__DR_CLR CYREG_GPIO_PRT0_DR_CLR
#define SPIM_mosi_m__0__DR_INV CYREG_GPIO_PRT0_DR_INV
#define SPIM_mosi_m__0__DR_SET CYREG_GPIO_PRT0_DR_SET
#define SPIM_mosi_m__0__HSIOM CYREG_HSIOM_PORT_SEL0
#define SPIM_mosi_m__0__HSIOM_GPIO 0u
#define SPIM_mosi_m__0__HSIOM_I2C 14u
#define SPIM_mosi_m__0__HSIOM_I2C_SDA 14u
#define SPIM_mosi_m__0__HSIOM_MASK 0x0000000Fu
#define SPIM_mosi_m__0__HSIOM_SHIFT 0u
#define SPIM_mosi_m__0__HSIOM_SPI 15u
#define SPIM_mosi_m__0__HSIOM_SPI_MOSI 15u
#define SPIM_mosi_m__0__HSIOM_UART 9u
#define SPIM_mosi_m__0__HSIOM_UART_RX 9u
#define SPIM_mosi_m__0__INTCFG CYREG_GPIO_PRT0_INTR_CFG
#define SPIM_mosi_m__0__INTR CYREG_GPIO_PRT0_INTR
#define SPIM_mosi_m__0__INTR_CFG CYREG_GPIO_PRT0_INTR_CFG
#define SPIM_mosi_m__0__INTSTAT CYREG_GPIO_PRT0_INTR
#define SPIM_mosi_m__0__MASK 0x01u
#define SPIM_mosi_m__0__OUT_SEL CYREG_UDB_PA0_CFG10
#define SPIM_mosi_m__0__OUT_SEL_SHIFT 0u
#define SPIM_mosi_m__0__OUT_SEL_VAL -1u
#define SPIM_mosi_m__0__PA__CFG0 CYREG_UDB_PA0_CFG0
#define SPIM_mosi_m__0__PA__CFG1 CYREG_UDB_PA0_CFG1
#define SPIM_mosi_m__0__PA__CFG10 CYREG_UDB_PA0_CFG10
#define SPIM_mosi_m__0__PA__CFG11 CYREG_UDB_PA0_CFG11
#define SPIM_mosi_m__0__PA__CFG12 CYREG_UDB_PA0_CFG12
#define SPIM_mosi_m__0__PA__CFG13 CYREG_UDB_PA0_CFG13
#define SPIM_mosi_m__0__PA__CFG14 CYREG_UDB_PA0_CFG14
#define SPIM_mosi_m__0__PA__CFG2 CYREG_UDB_PA0_CFG2
#define SPIM_mosi_m__0__PA__CFG3 CYREG_UDB_PA0_CFG3
#define SPIM_mosi_m__0__PA__CFG4 CYREG_UDB_PA0_CFG4
#define SPIM_mosi_m__0__PA__CFG5 CYREG_UDB_PA0_CFG5
#define SPIM_mosi_m__0__PA__CFG6 CYREG_UDB_PA0_CFG6
#define SPIM_mosi_m__0__PA__CFG7 CYREG_UDB_PA0_CFG7
#define SPIM_mosi_m__0__PA__CFG8 CYREG_UDB_PA0_CFG8
#define SPIM_mosi_m__0__PA__CFG9 CYREG_UDB_PA0_CFG9
#define SPIM_mosi_m__0__PC CYREG_GPIO_PRT0_PC
#define SPIM_mosi_m__0__PC2 CYREG_GPIO_PRT0_PC2
#define SPIM_mosi_m__0__PORT 0u
#define SPIM_mosi_m__0__PS CYREG_GPIO_PRT0_PS
#define SPIM_mosi_m__0__SHIFT 0u
#define SPIM_mosi_m__DR CYREG_GPIO_PRT0_DR
#define SPIM_mosi_m__DR_CLR CYREG_GPIO_PRT0_DR_CLR
#define SPIM_mosi_m__DR_INV CYREG_GPIO_PRT0_DR_INV
#define SPIM_mosi_m__DR_SET CYREG_GPIO_PRT0_DR_SET
#define SPIM_mosi_m__INTCFG CYREG_GPIO_PRT0_INTR_CFG
#define SPIM_mosi_m__INTR CYREG_GPIO_PRT0_INTR
#define SPIM_mosi_m__INTR_CFG CYREG_GPIO_PRT0_INTR_CFG
#define SPIM_mosi_m__INTSTAT CYREG_GPIO_PRT0_INTR
#define SPIM_mosi_m__MASK 0x01u
#define SPIM_mosi_m__PA__CFG0 CYREG_UDB_PA0_CFG0
#define SPIM_mosi_m__PA__CFG1 CYREG_UDB_PA0_CFG1
#define SPIM_mosi_m__PA__CFG10 CYREG_UDB_PA0_CFG10
#define SPIM_mosi_m__PA__CFG11 CYREG_UDB_PA0_CFG11
#define SPIM_mosi_m__PA__CFG12 CYREG_UDB_PA0_CFG12
#define SPIM_mosi_m__PA__CFG13 CYREG_UDB_PA0_CFG13
#define SPIM_mosi_m__PA__CFG14 CYREG_UDB_PA0_CFG14
#define SPIM_mosi_m__PA__CFG2 CYREG_UDB_PA0_CFG2
#define SPIM_mosi_m__PA__CFG3 CYREG_UDB_PA0_CFG3
#define SPIM_mosi_m__PA__CFG4 CYREG_UDB_PA0_CFG4
#define SPIM_mosi_m__PA__CFG5 CYREG_UDB_PA0_CFG5
#define SPIM_mosi_m__PA__CFG6 CYREG_UDB_PA0_CFG6
#define SPIM_mosi_m__PA__CFG7 CYREG_UDB_PA0_CFG7
#define SPIM_mosi_m__PA__CFG8 CYREG_UDB_PA0_CFG8
#define SPIM_mosi_m__PA__CFG9 CYREG_UDB_PA0_CFG9
#define SPIM_mosi_m__PC CYREG_GPIO_PRT0_PC
#define SPIM_mosi_m__PC2 CYREG_GPIO_PRT0_PC2
#define SPIM_mosi_m__PORT 0u
#define SPIM_mosi_m__PS CYREG_GPIO_PRT0_PS
#define SPIM_mosi_m__SHIFT 0u

/* SPIM_SCB */
#define SPIM_SCB__CTRL CYREG_SCB1_CTRL
#define SPIM_SCB__EZ_DATA0 CYREG_SCB1_EZ_DATA0
#define SPIM_SCB__EZ_DATA1 CYREG_SCB1_EZ_DATA1
#define SPIM_SCB__EZ_DATA10 CYREG_SCB1_EZ_DATA10
#define SPIM_SCB__EZ_DATA11 CYREG_SCB1_EZ_DATA11
#define SPIM_SCB__EZ_DATA12 CYREG_SCB1_EZ_DATA12
#define SPIM_SCB__EZ_DATA13 CYREG_SCB1_EZ_DATA13
#define SPIM_SCB__EZ_DATA14 CYREG_SCB1_EZ_DATA14
#define SPIM_SCB__EZ_DATA15 CYREG_SCB1_EZ_DATA15
#define SPIM_SCB__EZ_DATA16 CYREG_SCB1_EZ_DATA16
#define SPIM_SCB__EZ_DATA17 CYREG_SCB1_EZ_DATA17
#define SPIM_SCB__EZ_DATA18 CYREG_SCB1_EZ_DATA18
#define SPIM_SCB__EZ_DATA19 CYREG_SCB1_EZ_DATA19
#define SPIM_SCB__EZ_DATA2 CYREG_SCB1_EZ_DATA2
#define SPIM_SCB__EZ_DATA20 CYREG_SCB1_EZ_DATA20
#define SPIM_SCB__EZ_DATA21 CYREG_SCB1_EZ_DATA21
#define SPIM_SCB__EZ_DATA22 CYREG_SCB1_EZ_DATA22
#define SPIM_SCB__EZ_DATA23 CYREG_SCB1_EZ_DATA23
#define SPIM_SCB__EZ_DATA24 CYREG_SCB1_EZ_DATA24
#define SPIM_SCB__EZ_DATA25 CYREG_SCB1_EZ_DATA25
#define SPIM_SCB__EZ_DATA26 CYREG_SCB1_EZ_DATA26
#define SPIM_SCB__EZ_DATA27 CYREG_SCB1_EZ_DATA27
#define SPIM_SCB__EZ_DATA28 CYREG_SCB1_EZ_DATA28
#define SPIM_SCB__EZ_DATA29 CYREG_SCB1_EZ_DATA29
#define SPIM_SCB__EZ_DATA3 CYREG_SCB1_EZ_DATA3
#define SPIM_SCB__EZ_DATA30 CYREG_SCB1_EZ_DATA30
#define SPIM_SCB__EZ_DATA31 CYREG_SCB1_EZ_DATA31
#define SPIM_SCB__EZ_DATA4 CYREG_SCB1_EZ_DATA4
#define SPIM_SCB__EZ_DATA5 CYREG_SCB1_EZ_DATA5
#define SPIM_SCB__EZ_DATA6 CYREG_SCB1_EZ_DATA6
#define SPIM_SCB__EZ_DATA7 CYREG_SCB1_EZ_DATA7
#define SPIM_SCB__EZ_DATA8 CYREG_SCB1_EZ_DATA8
#define SPIM_SCB__EZ_DATA9 CYREG_SCB1_EZ_DATA9
#define SPIM_SCB__I2C_CFG CYREG_SCB1_I2C_CFG
#define SPIM_SCB__I2C_CTRL CYREG_SCB1_I2C_CTRL
#define SPIM_SCB__I2C_M_CMD CYREG_SCB1_I2C_M_CMD
#define SPIM_SCB__I2C_S_CMD CYREG_SCB1_I2C_S_CMD
#define SPIM_SCB__I2C_STATUS CYREG_SCB1_I2C_STATUS
#define SPIM_SCB__INTR_CAUSE CYREG_SCB1_INTR_CAUSE
#define SPIM_SCB__INTR_I2C_EC CYREG_SCB1_INTR_I2C_EC
#define SPIM_SCB__INTR_I2C_EC_MASK CYREG_SCB1_INTR_I2C_EC_MASK
#define SPIM_SCB__INTR_I2C_EC_MASKED CYREG_SCB1_INTR_I2C_EC_MASKED
#define SPIM_SCB__INTR_M CYREG_SCB1_INTR_M
#define SPIM_SCB__INTR_M_MASK CYREG_SCB1_INTR_M_MASK
#define SPIM_SCB__INTR_M_MASKED CYREG_SCB1_INTR_M_MASKED
#define SPIM_SCB__INTR_M_SET CYREG_SCB1_INTR_M_SET
#define SPIM_SCB__INTR_RX CYREG_SCB1_INTR_RX
#define SPIM_SCB__INTR_RX_MASK CYREG_SCB1_INTR_RX_MASK
#define SPIM_SCB__INTR_RX_MASKED CYREG_SCB1_INTR_RX_MASKED
#define SPIM_SCB__INTR_RX_SET CYREG_SCB1_INTR_RX_SET
#define SPIM_SCB__INTR_S CYREG_SCB1_INTR_S
#define SPIM_SCB__INTR_S_MASK CYREG_SCB1_INTR_S_MASK
#define SPIM_SCB__INTR_S_MASKED CYREG_SCB1_INTR_S_MASKED
#define SPIM_SCB__INTR_S_SET CYREG_SCB1_INTR_S_SET
#define SPIM_SCB__INTR_SPI_EC CYREG_SCB1_INTR_SPI_EC
#define SPIM_SCB__INTR_SPI_EC_MASK CYREG_SCB1_INTR_SPI_EC_MASK
#define SPIM_SCB__INTR_SPI_EC_MASKED CYREG_SCB1_INTR_SPI_EC_MASKED
#define SPIM_SCB__INTR_TX CYREG_SCB1_INTR_TX
#define SPIM_SCB__INTR_TX_MASK CYREG_SCB1_INTR_TX_MASK
#define SPIM_SCB__INTR_TX_MASKED CYREG_SCB1_INTR_TX_MASKED
#define SPIM_SCB__INTR_TX_SET CYREG_SCB1_INTR_TX_SET
#define SPIM_SCB__RX_CTRL CYREG_SCB1_RX_CTRL
#define SPIM_SCB__RX_FIFO_CTRL CYREG_SCB1_RX_FIFO_CTRL
#define SPIM_SCB__RX_FIFO_RD CYREG_SCB1_RX_FIFO_RD
#define SPIM_SCB__RX_FIFO_RD_SILENT CYREG_SCB1_RX_FIFO_RD_SILENT
#define SPIM_SCB__RX_FIFO_STATUS CYREG_SCB1_RX_FIFO_STATUS
#define SPIM_SCB__RX_MATCH CYREG_SCB1_RX_MATCH
#define SPIM_SCB__SPI_CTRL CYREG_SCB1_SPI_CTRL
#define SPIM_SCB__SPI_STATUS CYREG_SCB1_SPI_STATUS
#define SPIM_SCB__SS0_POSISTION 0u
#define SPIM_SCB__SS1_POSISTION 1u
#define SPIM_SCB__SS2_POSISTION 2u
#define SPIM_SCB__SS3_POSISTION 3u
#define SPIM_SCB__STATUS CYREG_SCB1_STATUS
#define SPIM_SCB__TX_CTRL CYREG_SCB1_TX_CTRL
#define SPIM_SCB__TX_FIFO_CTRL CYREG_SCB1_TX_FIFO_CTRL
#define SPIM_SCB__TX_FIFO_STATUS CYREG_SCB1_TX_FIFO_STATUS
#define SPIM_SCB__TX_FIFO_WR CYREG_SCB1_TX_FIFO_WR
#define SPIM_SCB__UART_CTRL CYREG_SCB1_UART_CTRL
#define SPIM_SCB__UART_FLOW_CTRL CYREG_SCB1_UART_FLOW_CTRL
#define SPIM_SCB__UART_RX_CTRL CYREG_SCB1_UART_RX_CTRL
#define SPIM_SCB__UART_RX_STATUS CYREG_SCB1_UART_RX_STATUS
#define SPIM_SCB__UART_TX_CTRL CYREG_SCB1_UART_TX_CTRL

/* SPIM_SCB_IRQ */
#define SPIM_SCB_IRQ__INTC_CLR_EN_REG CYREG_CM0_ICER
#define SPIM_SCB_IRQ__INTC_CLR_PD_REG CYREG_CM0_ICPR
#define SPIM_SCB_IRQ__INTC_MASK 0x400u
#define SPIM_SCB_IRQ__INTC_NUMBER 10u
#define SPIM_SCB_IRQ__INTC_PRIOR_MASK 0xC00000u
#define SPIM_SCB_IRQ__INTC_PRIOR_NUM 3u
#define SPIM_SCB_IRQ__INTC_PRIOR_REG CYREG_CM0_IPR2
#define SPIM_SCB_IRQ__INTC_SET_EN_REG CYREG_CM0_ISER
#define SPIM_SCB_IRQ__INTC_SET_PD_REG CYREG_CM0_ISPR

/* SPIM_SCBCLK */
#define SPIM_SCBCLK__CTRL_REGISTER CYREG_PERI_PCLK_CTL2
#define SPIM_SCBCLK__DIV_ID 0x00000040u
#define SPIM_SCBCLK__DIV_REGISTER CYREG_PERI_DIV_16_CTL0
#define SPIM_SCBCLK__PA_DIV_ID 0x000000FFu

/* SPIM_sclk_m */
#define SPIM_sclk_m__0__DR CYREG_GPIO_PRT0_DR
#define SPIM_sclk_m__0__DR_CLR CYREG_GPIO_PRT0_DR_CLR
#define SPIM_sclk_m__0__DR_INV CYREG_GPIO_PRT0_DR_INV
#define SPIM_sclk_m__0__DR_SET CYREG_GPIO_PRT0_DR_SET
#define SPIM_sclk_m__0__HSIOM CYREG_HSIOM_PORT_SEL0
#define SPIM_sclk_m__0__HSIOM_GPIO 0u
#define SPIM_sclk_m__0__HSIOM_MASK 0x0000F000u
#define SPIM_sclk_m__0__HSIOM_SHIFT 12u
#define SPIM_sclk_m__0__HSIOM_SPI 15u
#define SPIM_sclk_m__0__HSIOM_SPI_CLK 15u
#define SPIM_sclk_m__0__HSIOM_UART 9u
#define SPIM_sclk_m__0__HSIOM_UART_CTS 9u
#define SPIM_sclk_m__0__INTCFG CYREG_GPIO_PRT0_INTR_CFG
#define SPIM_sclk_m__0__INTR CYREG_GPIO_PRT0_INTR
#define SPIM_sclk_m__0__INTR_CFG CYREG_GPIO_PRT0_INTR_CFG
#define SPIM_sclk_m__0__INTSTAT CYREG_GPIO_PRT0_INTR
#define SPIM_sclk_m__0__MASK 0x08u
#define SPIM_sclk_m__0__OUT_SEL CYREG_UDB_PA0_CFG10
#define SPIM_sclk_m__0__OUT_SEL_SHIFT 6u
#define SPIM_sclk_m__0__OUT_SEL_VAL -1u
#define SPIM_sclk_m__0__PA__CFG0 CYREG_UDB_PA0_CFG0
#define SPIM_sclk_m__0__PA__CFG1 CYREG_UDB_PA0_CFG1
#define SPIM_sclk_m__0__PA__CFG10 CYREG_UDB_PA0_CFG10
#define SPIM_sclk_m__0__PA__CFG11 CYREG_UDB_PA0_CFG11
#define SPIM_sclk_m__0__PA__CFG12 CYREG_UDB_PA0_CFG12
#define SPIM_sclk_m__0__PA__CFG13 CYREG_UDB_PA0_CFG13
#define SPIM_sclk_m__0__PA__CFG14 CYREG_UDB_PA0_CFG14
#define SPIM_sclk_m__0__PA__CFG2 CYREG_UDB_PA0_CFG2
#define SPIM_sclk_m__0__PA__CFG3 CYREG_UDB_PA0_CFG3
#define SPIM_sclk_m__0__PA__CFG4 CYREG_UDB_PA0_CFG4
#define SPIM_sclk_m__0__PA__CFG5 CYREG_UDB_PA0_CFG5
#define SPIM_sclk_m__0__PA__CFG6 CYREG_UDB_PA0_CFG6
#define SPIM_sclk_m__0__PA__CFG7 CYREG_UDB_PA0_CFG7
#define SPIM_sclk_m__0__PA__CFG8 CYREG_UDB_PA0_CFG8
#define SPIM_sclk_m__0__PA__CFG9 CYREG_UDB_PA0_CFG9
#define SPIM_sclk_m__0__PC CYREG_GPIO_PRT0_PC
#define SPIM_sclk_m__0__PC2 CYREG_GPIO_PRT0_PC2
#define SPIM_sclk_m__0__PORT 0u
#define SPIM_sclk_m__0__PS CYREG_GPIO_PRT0_PS
#define SPIM_sclk_m__0__SHIFT 3u
#define SPIM_sclk_m__DR CYREG_GPIO_PRT0_DR
#define SPIM_sclk_m__DR_CLR CYREG_GPIO_PRT0_DR_CLR
#define SPIM_sclk_m__DR_INV CYREG_GPIO_PRT0_DR_INV
#define SPIM_sclk_m__DR_SET CYREG_GPIO_PRT0_DR_SET
#define SPIM_sclk_m__INTCFG CYREG_GPIO_PRT0_INTR_CFG
#define SPIM_sclk_m__INTR CYREG_GPIO_PRT0_INTR
#define SPIM_sclk_m__INTR_CFG CYREG_GPIO_PRT0_INTR_CFG
#define SPIM_sclk_m__INTSTAT CYREG_GPIO_PRT0_INTR
#define SPIM_sclk_m__MASK 0x08u
#define SPIM_sclk_m__PA__CFG0 CYREG_UDB_PA0_CFG0
#define SPIM_sclk_m__PA__CFG1 CYREG_UDB_PA0_CFG1
#define SPIM_sclk_m__PA__CFG10 CYREG_UDB_PA0_CFG10
#define SPIM_sclk_m__PA__CFG11 CYREG_UDB_PA0_CFG11
#define SPIM_sclk_m__PA__CFG12 CYREG_UDB_PA0_CFG12
#define SPIM_sclk_m__PA__CFG13 CYREG_UDB_PA0_CFG13
#define SPIM_sclk_m__PA__CFG14 CYREG_UDB_PA0_CFG14
#define SPIM_sclk_m__PA__CFG2 CYREG_UDB_PA0_CFG2
#define SPIM_sclk_m__PA__CFG3 CYREG_UDB_PA0_CFG3
#define SPIM_sclk_m__PA__CFG4 CYREG_UDB_PA0_CFG4
#define SPIM_sclk_m__PA__CFG5 CYREG_UDB_PA0_CFG5
#define SPIM_sclk_m__PA__CFG6 CYREG_UDB_PA0_CFG6
#define SPIM_sclk_m__PA__CFG7 CYREG_UDB_PA0_CFG7
#define SPIM_sclk_m__PA__CFG8 CYREG_UDB_PA0_CFG8
#define SPIM_sclk_m__PA__CFG9 CYREG_UDB_PA0_CFG9
#define SPIM_sclk_m__PC CYREG_GPIO_PRT0_PC
#define SPIM_sclk_m__PC2 CYREG_GPIO_PRT0_PC2
#define SPIM_sclk_m__PORT 0u
#define SPIM_sclk_m__PS CYREG_GPIO_PRT0_PS
#define SPIM_sclk_m__SHIFT 3u

/* SPIM_ss0_m */
#define SPIM_ss0_m__0__DR CYREG_GPIO_PRT0_DR
#define SPIM_ss0_m__0__DR_CLR CYREG_GPIO_PRT0_DR_CLR
#define SPIM_ss0_m__0__DR_INV CYREG_GPIO_PRT0_DR_INV
#define SPIM_ss0_m__0__DR_SET CYREG_GPIO_PRT0_DR_SET
#define SPIM_ss0_m__0__HSIOM CYREG_HSIOM_PORT_SEL0
#define SPIM_ss0_m__0__HSIOM_GPIO 0u
#define SPIM_ss0_m__0__HSIOM_MASK 0x00000F00u
#define SPIM_ss0_m__0__HSIOM_SHIFT 8u
#define SPIM_ss0_m__0__HSIOM_SPI 15u
#define SPIM_ss0_m__0__HSIOM_SPI_SELECT0 15u
#define SPIM_ss0_m__0__HSIOM_UART 9u
#define SPIM_ss0_m__0__HSIOM_UART_RTS 9u
#define SPIM_ss0_m__0__INTCFG CYREG_GPIO_PRT0_INTR_CFG
#define SPIM_ss0_m__0__INTR CYREG_GPIO_PRT0_INTR
#define SPIM_ss0_m__0__INTR_CFG CYREG_GPIO_PRT0_INTR_CFG
#define SPIM_ss0_m__0__INTSTAT CYREG_GPIO_PRT0_INTR
#define SPIM_ss0_m__0__MASK 0x04u
#define SPIM_ss0_m__0__OUT_SEL CYREG_UDB_PA0_CFG10
#define SPIM_ss0_m__0__OUT_SEL_SHIFT 4u
#define SPIM_ss0_m__0__OUT_SEL_VAL -1u
#define SPIM_ss0_m__0__PA__CFG0 CYREG_UDB_PA0_CFG0
#define SPIM_ss0_m__0__PA__CFG1 CYREG_UDB_PA0_CFG1
#define SPIM_ss0_m__0__PA__CFG10 CYREG_UDB_PA0_CFG10
#define SPIM_ss0_m__0__PA__CFG11 CYREG_UDB_PA0_CFG11
#define SPIM_ss0_m__0__PA__CFG12 CYREG_UDB_PA0_CFG12
#define SPIM_ss0_m__0__PA__CFG13 CYREG_UDB_PA0_CFG13
#define SPIM_ss0_m__0__PA__CFG14 CYREG_UDB_PA0_CFG14
#define SPIM_ss0_m__0__PA__CFG2 CYREG_UDB_PA0_CFG2
#define SPIM_ss0_m__0__PA__CFG3 CYREG_UDB_PA0_CFG3
#define SPIM_ss0_m__0__PA__CFG4 CYREG_UDB_PA0_CFG4
#define SPIM_ss0_m__0__PA__CFG5 CYREG_UDB_PA0_CFG5
#define SPIM_ss0_m__0__PA__CFG6 CYREG_UDB_PA0_CFG6
#define SPIM_ss0_m__0__PA__CFG7 CYREG_UDB_PA0_CFG7
#define SPIM_ss0_m__0__PA__CFG8 CYREG_UDB_PA0_CFG8
#define SPIM_ss0_m__0__PA__CFG9 CYREG_UDB_PA0_CFG9
#define SPIM_ss0_m__0__PC CYREG_GPIO_PRT0_PC
#define SPIM_ss0_m__0__PC2 CYREG_GPIO_PRT0_PC2
#define SPIM_ss0_m__0__PORT 0u
#define SPIM_ss0_m__0__PS CYREG_GPIO_PRT0_PS
#define SPIM_ss0_m__0__SHIFT 2u
#define SPIM_ss0_m__DR CYREG_GPIO_PRT0_DR
#define SPIM_ss0_m__DR_CLR CYREG_GPIO_PRT0_DR_CLR
#define SPIM_ss0_m__DR_INV CYREG_GPIO_PRT0_DR_INV
#define SPIM_ss0_m__DR_SET CYREG_GPIO_PRT0_DR_SET
#define SPIM_ss0_m__INTCFG CYREG_GPIO_PRT0_INTR_CFG
#define SPIM_ss0_m__INTR CYREG_GPIO_PRT0_INTR
#define SPIM_ss0_m__INTR_CFG CYREG_GPIO_PRT0_INTR_CFG
#define SPIM_ss0_m__INTSTAT CYREG_GPIO_PRT0_INTR
#define SPIM_ss0_m__MASK 0x04u
#define SPIM_ss0_m__PA__CFG0 CYREG_UDB_PA0_CFG0
#define SPIM_ss0_m__PA__CFG1 CYREG_UDB_PA0_CFG1
#define SPIM_ss0_m__PA__CFG10 CYREG_UDB_PA0_CFG10
#define SPIM_ss0_m__PA__CFG11 CYREG_UDB_PA0_CFG11
#define SPIM_ss0_m__PA__CFG12 CYREG_UDB_PA0_CFG12
#define SPIM_ss0_m__PA__CFG13 CYREG_UDB_PA0_CFG13
#define SPIM_ss0_m__PA__CFG14 CYREG_UDB_PA0_CFG14
#define SPIM_ss0_m__PA__CFG2 CYREG_UDB_PA0_CFG2
#define SPIM_ss0_m__PA__CFG3 CYREG_UDB_PA0_CFG3
#define SPIM_ss0_m__PA__CFG4 CYREG_UDB_PA0_CFG4
#define SPIM_ss0_m__PA__CFG5 CYREG_UDB_PA0_CFG5
#define SPIM_ss0_m__PA__CFG6 CYREG_UDB_PA0_CFG6
#define SPIM_ss0_m__PA__CFG7 CYREG_UDB_PA0_CFG7
#define SPIM_ss0_m__PA__CFG8 CYREG_UDB_PA0_CFG8
#define SPIM_ss0_m__PA__CFG9 CYREG_UDB_PA0_CFG9
#define SPIM_ss0_m__PC CYREG_GPIO_PRT0_PC
#define SPIM_ss0_m__PC2 CYREG_GPIO_PRT0_PC2
#define SPIM_ss0_m__PORT 0u
#define SPIM_ss0_m__PS CYREG_GPIO_PRT0_PS
#define SPIM_ss0_m__SHIFT 2u

/* ERROR */
#define ERROR__0__DR CYREG_GPIO_PRT2_DR
#define ERROR__0__DR_CLR CYREG_GPIO_PRT2_DR_CLR
#define ERROR__0__DR_INV CYREG_GPIO_PRT2_DR_INV
#define ERROR__0__DR_SET CYREG_GPIO_PRT2_DR_SET
#define ERROR__0__HSIOM CYREG_HSIOM_PORT_SEL2
#define ERROR__0__HSIOM_MASK 0x0F000000u
#define ERROR__0__HSIOM_SHIFT 24u
#define ERROR__0__INTCFG CYREG_GPIO_PRT2_INTR_CFG
#define ERROR__0__INTR CYREG_GPIO_PRT2_INTR
#define ERROR__0__INTR_CFG CYREG_GPIO_PRT2_INTR_CFG
#define ERROR__0__INTSTAT CYREG_GPIO_PRT2_INTR
#define ERROR__0__MASK 0x40u
#define ERROR__0__PA__CFG0 CYREG_UDB_PA2_CFG0
#define ERROR__0__PA__CFG1 CYREG_UDB_PA2_CFG1
#define ERROR__0__PA__CFG10 CYREG_UDB_PA2_CFG10
#define ERROR__0__PA__CFG11 CYREG_UDB_PA2_CFG11
#define ERROR__0__PA__CFG12 CYREG_UDB_PA2_CFG12
#define ERROR__0__PA__CFG13 CYREG_UDB_PA2_CFG13
#define ERROR__0__PA__CFG14 CYREG_UDB_PA2_CFG14
#define ERROR__0__PA__CFG2 CYREG_UDB_PA2_CFG2
#define ERROR__0__PA__CFG3 CYREG_UDB_PA2_CFG3
#define ERROR__0__PA__CFG4 CYREG_UDB_PA2_CFG4
#define ERROR__0__PA__CFG5 CYREG_UDB_PA2_CFG5
#define ERROR__0__PA__CFG6 CYREG_UDB_PA2_CFG6
#define ERROR__0__PA__CFG7 CYREG_UDB_PA2_CFG7
#define ERROR__0__PA__CFG8 CYREG_UDB_PA2_CFG8
#define ERROR__0__PA__CFG9 CYREG_UDB_PA2_CFG9
#define ERROR__0__PC CYREG_GPIO_PRT2_PC
#define ERROR__0__PC2 CYREG_GPIO_PRT2_PC2
#define ERROR__0__PORT 2u
#define ERROR__0__PS CYREG_GPIO_PRT2_PS
#define ERROR__0__SHIFT 6u
#define ERROR__DR CYREG_GPIO_PRT2_DR
#define ERROR__DR_CLR CYREG_GPIO_PRT2_DR_CLR
#define ERROR__DR_INV CYREG_GPIO_PRT2_DR_INV
#define ERROR__DR_SET CYREG_GPIO_PRT2_DR_SET
#define ERROR__INTCFG CYREG_GPIO_PRT2_INTR_CFG
#define ERROR__INTR CYREG_GPIO_PRT2_INTR
#define ERROR__INTR_CFG CYREG_GPIO_PRT2_INTR_CFG
#define ERROR__INTSTAT CYREG_GPIO_PRT2_INTR
#define ERROR__MASK 0x40u
#define ERROR__PA__CFG0 CYREG_UDB_PA2_CFG0
#define ERROR__PA__CFG1 CYREG_UDB_PA2_CFG1
#define ERROR__PA__CFG10 CYREG_UDB_PA2_CFG10
#define ERROR__PA__CFG11 CYREG_UDB_PA2_CFG11
#define ERROR__PA__CFG12 CYREG_UDB_PA2_CFG12
#define ERROR__PA__CFG13 CYREG_UDB_PA2_CFG13
#define ERROR__PA__CFG14 CYREG_UDB_PA2_CFG14
#define ERROR__PA__CFG2 CYREG_UDB_PA2_CFG2
#define ERROR__PA__CFG3 CYREG_UDB_PA2_CFG3
#define ERROR__PA__CFG4 CYREG_UDB_PA2_CFG4
#define ERROR__PA__CFG5 CYREG_UDB_PA2_CFG5
#define ERROR__PA__CFG6 CYREG_UDB_PA2_CFG6
#define ERROR__PA__CFG7 CYREG_UDB_PA2_CFG7
#define ERROR__PA__CFG8 CYREG_UDB_PA2_CFG8
#define ERROR__PA__CFG9 CYREG_UDB_PA2_CFG9
#define ERROR__PC CYREG_GPIO_PRT2_PC
#define ERROR__PC2 CYREG_GPIO_PRT2_PC2
#define ERROR__PORT 2u
#define ERROR__PS CYREG_GPIO_PRT2_PS
#define ERROR__SHIFT 6u

/* RxDmaM */
#define RxDmaM__CH_CTL CYREG_DMAC_CH_CTL1
#define RxDmaM__CHANNEL_NUMBER 1
#define RxDmaM__DESCR_PING_CTL CYREG_DMAC_DESCR1_PING_CTL
#define RxDmaM__DESCR_PING_DST CYREG_DMAC_DESCR1_PING_DST
#define RxDmaM__DESCR_PING_SRC CYREG_DMAC_DESCR1_PING_SRC
#define RxDmaM__DESCR_PING_STATUS CYREG_DMAC_DESCR1_PING_STATUS
#define RxDmaM__DESCR_PONG_CTL CYREG_DMAC_DESCR1_PONG_CTL
#define RxDmaM__DESCR_PONG_DST CYREG_DMAC_DESCR1_PONG_DST
#define RxDmaM__DESCR_PONG_SRC CYREG_DMAC_DESCR1_PONG_SRC
#define RxDmaM__DESCR_PONG_STATUS CYREG_DMAC_DESCR1_PONG_STATUS
#define RxDmaM__PRIORITY 3
#define RxDmaM__TR_GROUP 0u
#define RxDmaM__TR_OUTPUT 1u

/* TxDmaM */
#define TxDmaM__CH_CTL CYREG_DMAC_CH_CTL0
#define TxDmaM__CHANNEL_NUMBER 0
#define TxDmaM__DESCR_PING_CTL CYREG_DMAC_DESCR0_PING_CTL
#define TxDmaM__DESCR_PING_DST CYREG_DMAC_DESCR0_PING_DST
#define TxDmaM__DESCR_PING_SRC CYREG_DMAC_DESCR0_PING_SRC
#define TxDmaM__DESCR_PING_STATUS CYREG_DMAC_DESCR0_PING_STATUS
#define TxDmaM__DESCR_PONG_CTL CYREG_DMAC_DESCR0_PONG_CTL
#define TxDmaM__DESCR_PONG_DST CYREG_DMAC_DESCR0_PONG_DST
#define TxDmaM__DESCR_PONG_SRC CYREG_DMAC_DESCR0_PONG_SRC
#define TxDmaM__DESCR_PONG_STATUS CYREG_DMAC_DESCR0_PONG_STATUS
#define TxDmaM__PRIORITY 3
#define TxDmaM__TR_GROUP 0u
#define TxDmaM__TR_OUTPUT 0u

/* SUCCESS */
#define SUCCESS__0__DR CYREG_GPIO_PRT3_DR
#define SUCCESS__0__DR_CLR CYREG_GPIO_PRT3_DR_CLR
#define SUCCESS__0__DR_INV CYREG_GPIO_PRT3_DR_INV
#define SUCCESS__0__DR_SET CYREG_GPIO_PRT3_DR_SET
#define SUCCESS__0__HSIOM CYREG_HSIOM_PORT_SEL3
#define SUCCESS__0__HSIOM_MASK 0x0F000000u
#define SUCCESS__0__HSIOM_SHIFT 24u
#define SUCCESS__0__INTCFG CYREG_GPIO_PRT3_INTR_CFG
#define SUCCESS__0__INTR CYREG_GPIO_PRT3_INTR
#define SUCCESS__0__INTR_CFG CYREG_GPIO_PRT3_INTR_CFG
#define SUCCESS__0__INTSTAT CYREG_GPIO_PRT3_INTR
#define SUCCESS__0__MASK 0x40u
#define SUCCESS__0__PA__CFG0 CYREG_UDB_PA3_CFG0
#define SUCCESS__0__PA__CFG1 CYREG_UDB_PA3_CFG1
#define SUCCESS__0__PA__CFG10 CYREG_UDB_PA3_CFG10
#define SUCCESS__0__PA__CFG11 CYREG_UDB_PA3_CFG11
#define SUCCESS__0__PA__CFG12 CYREG_UDB_PA3_CFG12
#define SUCCESS__0__PA__CFG13 CYREG_UDB_PA3_CFG13
#define SUCCESS__0__PA__CFG14 CYREG_UDB_PA3_CFG14
#define SUCCESS__0__PA__CFG2 CYREG_UDB_PA3_CFG2
#define SUCCESS__0__PA__CFG3 CYREG_UDB_PA3_CFG3
#define SUCCESS__0__PA__CFG4 CYREG_UDB_PA3_CFG4
#define SUCCESS__0__PA__CFG5 CYREG_UDB_PA3_CFG5
#define SUCCESS__0__PA__CFG6 CYREG_UDB_PA3_CFG6
#define SUCCESS__0__PA__CFG7 CYREG_UDB_PA3_CFG7
#define SUCCESS__0__PA__CFG8 CYREG_UDB_PA3_CFG8
#define SUCCESS__0__PA__CFG9 CYREG_UDB_PA3_CFG9
#define SUCCESS__0__PC CYREG_GPIO_PRT3_PC
#define SUCCESS__0__PC2 CYREG_GPIO_PRT3_PC2
#define SUCCESS__0__PORT 3u
#define SUCCESS__0__PS CYREG_GPIO_PRT3_PS
#define SUCCESS__0__SHIFT 6u
#define SUCCESS__DR CYREG_GPIO_PRT3_DR
#define SUCCESS__DR_CLR CYREG_GPIO_PRT3_DR_CLR
#define SUCCESS__DR_INV CYREG_GPIO_PRT3_DR_INV
#define SUCCESS__DR_SET CYREG_GPIO_PRT3_DR_SET
#define SUCCESS__INTCFG CYREG_GPIO_PRT3_INTR_CFG
#define SUCCESS__INTR CYREG_GPIO_PRT3_INTR
#define SUCCESS__INTR_CFG CYREG_GPIO_PRT3_INTR_CFG
#define SUCCESS__INTSTAT CYREG_GPIO_PRT3_INTR
#define SUCCESS__MASK 0x40u
#define SUCCESS__PA__CFG0 CYREG_UDB_PA3_CFG0
#define SUCCESS__PA__CFG1 CYREG_UDB_PA3_CFG1
#define SUCCESS__PA__CFG10 CYREG_UDB_PA3_CFG10
#define SUCCESS__PA__CFG11 CYREG_UDB_PA3_CFG11
#define SUCCESS__PA__CFG12 CYREG_UDB_PA3_CFG12
#define SUCCESS__PA__CFG13 CYREG_UDB_PA3_CFG13
#define SUCCESS__PA__CFG14 CYREG_UDB_PA3_CFG14
#define SUCCESS__PA__CFG2 CYREG_UDB_PA3_CFG2
#define SUCCESS__PA__CFG3 CYREG_UDB_PA3_CFG3
#define SUCCESS__PA__CFG4 CYREG_UDB_PA3_CFG4
#define SUCCESS__PA__CFG5 CYREG_UDB_PA3_CFG5
#define SUCCESS__PA__CFG6 CYREG_UDB_PA3_CFG6
#define SUCCESS__PA__CFG7 CYREG_UDB_PA3_CFG7
#define SUCCESS__PA__CFG8 CYREG_UDB_PA3_CFG8
#define SUCCESS__PA__CFG9 CYREG_UDB_PA3_CFG9
#define SUCCESS__PC CYREG_GPIO_PRT3_PC
#define SUCCESS__PC2 CYREG_GPIO_PRT3_PC2
#define SUCCESS__PORT 3u
#define SUCCESS__PS CYREG_GPIO_PRT3_PS
#define SUCCESS__SHIFT 6u

/* Miscellaneous */
#define CY_PROJECT_NAME "DMA_SPI_PSoC401_24L04p"
#define CY_VERSION "PSoC Creator  3.3 CP3"
#define CYDEV_BANDGAP_VOLTAGE 1.024
#define CYDEV_BCLK__HFCLK__HZ 24000000U
#define CYDEV_BCLK__HFCLK__KHZ 24000U
#define CYDEV_BCLK__HFCLK__MHZ 24U
#define CYDEV_BCLK__SYSCLK__HZ 24000000U
#define CYDEV_BCLK__SYSCLK__KHZ 24000U
#define CYDEV_BCLK__SYSCLK__MHZ 24U
#define CYDEV_CHIP_DIE_LEOPARD 1u
#define CYDEV_CHIP_DIE_PANTHER 19u
#define CYDEV_CHIP_DIE_PSOC4A 11u
#define CYDEV_CHIP_DIE_PSOC5LP 18u
#define CYDEV_CHIP_DIE_TMA4 2u
#define CYDEV_CHIP_DIE_UNKNOWN 0u
#define CYDEV_CHIP_FAMILY_PSOC3 1u
#define CYDEV_CHIP_FAMILY_PSOC4 2u
#define CYDEV_CHIP_FAMILY_PSOC5 3u
#define CYDEV_CHIP_FAMILY_UNKNOWN 0u
#define CYDEV_CHIP_FAMILY_USED CYDEV_CHIP_FAMILY_PSOC4
#define CYDEV_CHIP_JTAG_ID 0x1A1711AAu
#define CYDEV_CHIP_MEMBER_3A 1u
#define CYDEV_CHIP_MEMBER_4A 11u
#define CYDEV_CHIP_MEMBER_4C 16u
#define CYDEV_CHIP_MEMBER_4D 7u
#define CYDEV_CHIP_MEMBER_4E 4u
#define CYDEV_CHIP_MEMBER_4F 12u
#define CYDEV_CHIP_MEMBER_4G 2u
#define CYDEV_CHIP_MEMBER_4H 10u
#define CYDEV_CHIP_MEMBER_4I 15u
#define CYDEV_CHIP_MEMBER_4J 8u
#define CYDEV_CHIP_MEMBER_4K 9u
#define CYDEV_CHIP_MEMBER_4L 14u
#define CYDEV_CHIP_MEMBER_4M 13u
#define CYDEV_CHIP_MEMBER_4N 6u
#define CYDEV_CHIP_MEMBER_4O 5u
#define CYDEV_CHIP_MEMBER_4U 3u
#define CYDEV_CHIP_MEMBER_5A 18u
#define CYDEV_CHIP_MEMBER_5B 17u
#define CYDEV_CHIP_MEMBER_UNKNOWN 0u
#define CYDEV_CHIP_MEMBER_USED CYDEV_CHIP_MEMBER_4F
#define CYDEV_CHIP_DIE_EXPECT CYDEV_CHIP_MEMBER_USED
#define CYDEV_CHIP_DIE_ACTUAL CYDEV_CHIP_DIE_EXPECT
#define CYDEV_CHIP_REV_LEOPARD_ES1 0u
#define CYDEV_CHIP_REV_LEOPARD_ES2 1u
#define CYDEV_CHIP_REV_LEOPARD_ES3 3u
#define CYDEV_CHIP_REV_LEOPARD_PRODUCTION 3u
#define CYDEV_CHIP_REV_PANTHER_ES0 0u
#define CYDEV_CHIP_REV_PANTHER_ES1 1u
#define CYDEV_CHIP_REV_PANTHER_PRODUCTION 1u
#define CYDEV_CHIP_REV_PSOC4A_ES0 17u
#define CYDEV_CHIP_REV_PSOC4A_PRODUCTION 17u
#define CYDEV_CHIP_REV_PSOC5LP_ES0 0u
#define CYDEV_CHIP_REV_PSOC5LP_PRODUCTION 0u
#define CYDEV_CHIP_REV_TMA4_ES 17u
#define CYDEV_CHIP_REV_TMA4_ES2 33u
#define CYDEV_CHIP_REV_TMA4_PRODUCTION 17u
#define CYDEV_CHIP_REVISION_3A_ES1 0u
#define CYDEV_CHIP_REVISION_3A_ES2 1u
#define CYDEV_CHIP_REVISION_3A_ES3 3u
#define CYDEV_CHIP_REVISION_3A_PRODUCTION 3u
#define CYDEV_CHIP_REVISION_4A_ES0 17u
#define CYDEV_CHIP_REVISION_4A_PRODUCTION 17u
#define CYDEV_CHIP_REVISION_4C_PRODUCTION 0u
#define CYDEV_CHIP_REVISION_4D_PRODUCTION 0u
#define CYDEV_CHIP_REVISION_4E_PRODUCTION 0u
#define CYDEV_CHIP_REVISION_4F_PRODUCTION 0u
#define CYDEV_CHIP_REVISION_4F_PRODUCTION_256DMA 0u
#define CYDEV_CHIP_REVISION_4F_PRODUCTION_256K 0u
#define CYDEV_CHIP_REVISION_4G_ES 17u
#define CYDEV_CHIP_REVISION_4G_ES2 33u
#define CYDEV_CHIP_REVISION_4G_PRODUCTION 17u
#define CYDEV_CHIP_REVISION_4H_PRODUCTION 0u
#define CYDEV_CHIP_REVISION_4I_PRODUCTION 0u
#define CYDEV_CHIP_REVISION_4J_PRODUCTION 0u
#define CYDEV_CHIP_REVISION_4K_PRODUCTION 0u
#define CYDEV_CHIP_REVISION_4L_PRODUCTION 0u
#define CYDEV_CHIP_REVISION_4M_PRODUCTION 0u
#define CYDEV_CHIP_REVISION_4N_PRODUCTION 0u
#define CYDEV_CHIP_REVISION_4O_PRODUCTION 0u
#define CYDEV_CHIP_REVISION_4U_PRODUCTION 0u
#define CYDEV_CHIP_REVISION_5A_ES0 0u
#define CYDEV_CHIP_REVISION_5A_ES1 1u
#define CYDEV_CHIP_REVISION_5A_PRODUCTION 1u
#define CYDEV_CHIP_REVISION_5B_ES0 0u
#define CYDEV_CHIP_REVISION_5B_PRODUCTION 0u
#define CYDEV_CHIP_REVISION_USED CYDEV_CHIP_REVISION_4F_PRODUCTION_256DMA
#define CYDEV_CHIP_REV_EXPECT CYDEV_CHIP_REVISION_USED
#define CYDEV_CONFIG_READ_ACCELERATOR 1
#define CYDEV_CONFIG_UNUSED_IO_AllowButWarn 0
#define CYDEV_CONFIG_UNUSED_IO CYDEV_CONFIG_UNUSED_IO_AllowButWarn
#define CYDEV_CONFIG_UNUSED_IO_AllowWithInfo 1
#define CYDEV_CONFIG_UNUSED_IO_Disallowed 2
#define CYDEV_CONFIGURATION_COMPRESSED 1
#define CYDEV_CONFIGURATION_MODE_COMPRESSED 0
#define CYDEV_CONFIGURATION_MODE CYDEV_CONFIGURATION_MODE_COMPRESSED
#define CYDEV_CONFIGURATION_MODE_DMA 2
#define CYDEV_CONFIGURATION_MODE_UNCOMPRESSED 1
#define CYDEV_DEBUG_PROTECT_KILL 4
#define CYDEV_DEBUG_PROTECT_OPEN 1
#define CYDEV_DEBUG_PROTECT CYDEV_DEBUG_PROTECT_OPEN
#define CYDEV_DEBUG_PROTECT_PROTECTED 2
#define CYDEV_DEBUGGING_DPS_Disable 3
#define CYDEV_DEBUGGING_DPS_SWD 2
#define CYDEV_DEBUGGING_DPS CYDEV_DEBUGGING_DPS_SWD
#define CYDEV_DEBUGGING_ENABLE 1
#define CYDEV_DFT_SELECT_CLK0 10u
#define CYDEV_DFT_SELECT_CLK1 11u
#define CYDEV_DMA_CHANNELS_AVAILABLE 8
#define CYDEV_HEAP_SIZE 0x80
#define CYDEV_IMO_TRIMMED_BY_USB 0u
#define CYDEV_IMO_TRIMMED_BY_WCO 0u
#define CYDEV_INTR_NUMBER_DMA 21u
#define CYDEV_IS_EXPORTING_CODE 0
#define CYDEV_IS_IMPORTING_CODE 0
#define CYDEV_PROJ_TYPE 0
#define CYDEV_PROJ_TYPE_BOOTLOADER 1
#define CYDEV_PROJ_TYPE_LAUNCHER 5
#define CYDEV_PROJ_TYPE_LOADABLE 2
#define CYDEV_PROJ_TYPE_LOADABLEANDBOOTLOADER 4
#define CYDEV_PROJ_TYPE_MULTIAPPBOOTLOADER 3
#define CYDEV_PROJ_TYPE_STANDARD 0
#define CYDEV_STACK_SIZE 0x0800
#define CYDEV_USE_BUNDLED_CMSIS 1
#define CYDEV_VARIABLE_VDDA 1
#define CYDEV_VDDA 3.3
#define CYDEV_VDDA_MV 3300
#define CYDEV_VDDD 3.3
#define CYDEV_VDDD_MV 3300
#define CYDEV_VDDR 3.3
#define CYDEV_VDDR_MV 3300
#define CYDEV_WDT_GENERATE_ISR 1u
#define CYIPBLOCK_m0s8bless_VERSION 2
#define CYIPBLOCK_m0s8cpussv2_VERSION 1
#define CYIPBLOCK_m0s8csd_VERSION 1
#define CYIPBLOCK_m0s8ioss_VERSION 1
#define CYIPBLOCK_m0s8lcd_VERSION 2
#define CYIPBLOCK_m0s8lpcomp_VERSION 2
#define CYIPBLOCK_m0s8peri_VERSION 1
#define CYIPBLOCK_m0s8scb_VERSION 2
#define CYIPBLOCK_m0s8srssv2_VERSION 1
#define CYIPBLOCK_m0s8tcpwm_VERSION 2
#define CYIPBLOCK_m0s8udbif_VERSION 1
#define CYIPBLOCK_s8pass4al_VERSION 1
#define DMA_CHANNELS_USED__MASK 3u
#define CYDEV_BOOTLOADER_ENABLE 0

#endif /* INCLUDED_CYFITTER_H */
