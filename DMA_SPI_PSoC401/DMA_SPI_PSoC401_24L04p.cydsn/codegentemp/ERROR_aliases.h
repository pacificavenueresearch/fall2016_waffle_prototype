/*******************************************************************************
* File Name: ERROR.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_ERROR_ALIASES_H) /* Pins ERROR_ALIASES_H */
#define CY_PINS_ERROR_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define ERROR_0			(ERROR__0__PC)
#define ERROR_0_PS		(ERROR__0__PS)
#define ERROR_0_PC		(ERROR__0__PC)
#define ERROR_0_DR		(ERROR__0__DR)
#define ERROR_0_SHIFT	(ERROR__0__SHIFT)
#define ERROR_0_INTR	((uint16)((uint16)0x0003u << (ERROR__0__SHIFT*2u)))

#define ERROR_INTR_ALL	 ((uint16)(ERROR_0_INTR))


#endif /* End Pins ERROR_ALIASES_H */


/* [] END OF FILE */
