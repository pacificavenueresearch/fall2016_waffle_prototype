/*******************************************************************************
* File Name: SUCCESS.c  
* Version 2.20
*
* Description:
*  This file contains APIs to set up the Pins component for low power modes.
*
* Note:
*
********************************************************************************
* Copyright 2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "SUCCESS.h"

static SUCCESS_BACKUP_STRUCT  SUCCESS_backup = {0u, 0u, 0u};


/*******************************************************************************
* Function Name: SUCCESS_Sleep
****************************************************************************//**
*
* \brief Stores the pin configuration and prepares the pin for entering chip 
*  deep-sleep/hibernate modes. This function must be called for SIO and USBIO
*  pins. It is not essential if using GPIO or GPIO_OVT pins.
*
* <b>Note</b> This function is available in PSoC 4 only.
*
* \return 
*  None 
*  
* \sideeffect
*  For SIO pins, this function configures the pin input threshold to CMOS and
*  drive level to Vddio. This is needed for SIO pins when in device 
*  deep-sleep/hibernate modes.
*
* \funcusage
*  \snippet SUCCESS_SUT.c usage_SUCCESS_Sleep_Wakeup
*******************************************************************************/
void SUCCESS_Sleep(void)
{
    #if defined(SUCCESS__PC)
        SUCCESS_backup.pcState = SUCCESS_PC;
    #else
        #if (CY_PSOC4_4200L)
            /* Save the regulator state and put the PHY into suspend mode */
            SUCCESS_backup.usbState = SUCCESS_CR1_REG;
            SUCCESS_USB_POWER_REG |= SUCCESS_USBIO_ENTER_SLEEP;
            SUCCESS_CR1_REG &= SUCCESS_USBIO_CR1_OFF;
        #endif
    #endif
    #if defined(CYIPBLOCK_m0s8ioss_VERSION) && defined(SUCCESS__SIO)
        SUCCESS_backup.sioState = SUCCESS_SIO_REG;
        /* SIO requires unregulated output buffer and single ended input buffer */
        SUCCESS_SIO_REG &= (uint32)(~SUCCESS_SIO_LPM_MASK);
    #endif  
}


/*******************************************************************************
* Function Name: SUCCESS_Wakeup
****************************************************************************//**
*
* \brief Restores the pin configuration that was saved during Pin_Sleep().
*
* For USBIO pins, the wakeup is only triggered for falling edge interrupts.
*
* <b>Note</b> This function is available in PSoC 4 only.
*
* \return 
*  None
*  
* \funcusage
*  Refer to SUCCESS_Sleep() for an example usage.
*******************************************************************************/
void SUCCESS_Wakeup(void)
{
    #if defined(SUCCESS__PC)
        SUCCESS_PC = SUCCESS_backup.pcState;
    #else
        #if (CY_PSOC4_4200L)
            /* Restore the regulator state and come out of suspend mode */
            SUCCESS_USB_POWER_REG &= SUCCESS_USBIO_EXIT_SLEEP_PH1;
            SUCCESS_CR1_REG = SUCCESS_backup.usbState;
            SUCCESS_USB_POWER_REG &= SUCCESS_USBIO_EXIT_SLEEP_PH2;
        #endif
    #endif
    #if defined(CYIPBLOCK_m0s8ioss_VERSION) && defined(SUCCESS__SIO)
        SUCCESS_SIO_REG = SUCCESS_backup.sioState;
    #endif
}


/* [] END OF FILE */
