-- ======================================================================
-- DMA_SPI_PSoC401.ctl generated from DMA_SPI_PSoC401
-- 09/23/2016 at 15:07
-- This file is auto generated. ANY EDITS YOU MAKE MAY BE LOST WHEN THIS FILE IS REGENERATED!!!
-- ======================================================================

-- SPIS

-- TopDesign
-- =============================================================================
-- The following directives assign pins to the locations specific for the
-- CY8CKIT-042-BLE kit.
-- =============================================================================

-- === UART ===
attribute port_location of \UART:rx(0)\ : label is "PORT(1,4)";
attribute port_location of \UART:tx(0)\ : label is "PORT(1,5)";

-- === I2C ===
attribute port_location of \I2C:scl(0)\ : label is "PORT(3,5)";
attribute port_location of \I2C:sda(0)\ : label is "PORT(3,4)";

-- === RGB LED ===
attribute port_location of ERROR(0) : label is "PORT(2,6)"; -- RED LED
attribute port_location of SUCCESS(0) : label is "PORT(3,6)"; -- GREEN LED
attribute port_location of REPLACE_WITH_ACTUAL_PIN_NAME(0) : label is "PORT(3,7)"; -- BLUE LED

-- === USER SWITCH ===
attribute port_location of REPLACE_WITH_ACTUAL_PIN_NAME(0) : label is "PORT(0,7)";

-- === SPI Master ===
attribute port_location of \SPIM:miso_m(0)\ : label is "PORT(0,1)";
attribute port_location of \SPIM:mosi_m(0)\ : label is "PORT(0,0)";
attribute port_location of \SPIM:sclk_m(0)\ : label is "PORT(0,3)";
attribute port_location of \SPIM:ss0_m(0)\ : label is "PORT(0,2)";

-- === SPI Slave ===
attribute port_location of MISO_S(0) : label is "PORT(1,3)";
attribute port_location of MOSI_S(0) : label is "PORT(1,2)";
attribute port_location of SCLK_S(0) : label is "PORT(1,0)";
attribute port_location of SS_S(0)   : label is "PORT(1,1)";
-- M0S8 Clock Editor
-- Directives Editor
-- Analog Device Editor
