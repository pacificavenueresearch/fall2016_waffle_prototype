/*******************************************************************************
* File Name: SUCCESS.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_SUCCESS_ALIASES_H) /* Pins SUCCESS_ALIASES_H */
#define CY_PINS_SUCCESS_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define SUCCESS_0			(SUCCESS__0__PC)
#define SUCCESS_0_PS		(SUCCESS__0__PS)
#define SUCCESS_0_PC		(SUCCESS__0__PC)
#define SUCCESS_0_DR		(SUCCESS__0__DR)
#define SUCCESS_0_SHIFT	(SUCCESS__0__SHIFT)
#define SUCCESS_0_INTR	((uint16)((uint16)0x0003u << (SUCCESS__0__SHIFT*2u)))

#define SUCCESS_INTR_ALL	 ((uint16)(SUCCESS_0_INTR))


#endif /* End Pins SUCCESS_ALIASES_H */


/* [] END OF FILE */
